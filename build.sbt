name := """une-booster-register"""

organization := "com.nagytech"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.11"

herokuAppName in Compile := "gentle-savannah-67466"

val appDependencies = Seq(
  cache,
  filters,
  javaWs,
  javaJdbc,
  "be.objectify"      %% "deadbolt-java"        % "2.5.0",
  "com.feth"          %% "play-authenticate"    % "0.8.1",
  "com.feth"          %% "play-easymail"        % "0.8.1",
  "mysql"             % "mysql-connector-java"  % "5.1.18",
  "org.apache.poi"    % "poi"                   % "3.15",
  "org.apache.poi"    % "poi-ooxml"             % "3.15",
  "org.webjars"       % "bootstrap"             % "3.2.0",
  "org.webjars"       % "react"                 % "15.3.2",
  "org.webjars"       %% "webjars-play"         % "2.5.0"
)

lazy val root = (project in file("."))
  .enablePlugins(PlayJava, PlayEbean, SbtWeb)
  .settings(
      libraryDependencies ++= appDependencies
  )
