// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.13")

addSbtPlugin("com.typesafe.sbt" %% "sbt-play-ebean" % "3.0.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-rjs" % "1.0.7")

addSbtPlugin("com.github.ddispaltro" % "sbt-reactjs" % "0.6.8")

addSbtPlugin("com.heroku" % "sbt-heroku" % "1.0.1")

dependencyOverrides += "org.webjars.npm" % "minimatch" % "3.0.4"
