server {
    listen 80;
    listen [::]:80;
    listen 443 ssl;

    proxy_http_version 1.1; 

    server_name unehscboosterdays.com;

    ssl_certificate /etc/ssl/certs/unehscboosterdays.com.crt;
    ssl_certificate_key /etc/ssl/certs/unehscboosterdays.com.key;

    if ($scheme = http) {
        return 301 https://$server_name$request_uri;
    } 

    location ^~ /offline/lib {
        alias /opt/unehscboosterdays.com/target/web/web-modules/main/webjars/lib/;
    }
	  location ^~ /offline/pub {
        alias /opt/unehscboosterdays.com/public/;
    }
    
    location / {
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:9431;
        proxy_connect_timeout 5;
        proxy_next_upstream error timeout http_500 http_502 http_503 http_504;
        proxy_intercept_errors on;
    }

    error_page 500 502 503 504 /offline.html;
    location = /offline.html {
        root /opt/unehscboosterdays.com/public;
    }

}
