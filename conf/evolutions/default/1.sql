# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table linked_account (
  id                            bigint not null,
  user_id                       bigint,
  provider_user_id              varchar(255),
  provider_key                  varchar(255),
  constraint pk_linked_account primary key (id)
);
create sequence linked_account_seq;

create table schools (
  id                            bigint not null,
  name                          varchar(255),
  street_address                varchar(255),
  town_suburb                   varchar(255),
  state                         varchar(255),
  post_code                     varchar(255),
  created_on                    timestamp,
  modified_on                   timestamp,
  teacher_id                    bigint,
  constraint uq_schools_teacher_id unique (teacher_id),
  constraint pk_schools primary key (id)
);
create sequence schools_seq;

create table security_role (
  id                            bigint not null,
  role_name                     varchar(255),
  constraint pk_security_role primary key (id)
);
create sequence security_role_seq;

create table sessions (
  id                            bigint not null,
  name                          varchar(255),
  description                   varchar(255),
  location                      varchar(255),
  capacity                      integer,
  timeslot_id                   bigint,
  created_on                    timestamp,
  modified_on                   timestamp,
  deactivated                   boolean,
  constraint pk_sessions primary key (id)
);
create sequence sessions_seq;

create table session_times (
  id                            bigint not null,
  name                          varchar(255),
  code                          varchar(255),
  start_time                    timestamp,
  end_time                      timestamp,
  deactivated                   boolean,
  constraint pk_session_times primary key (id)
);
create sequence session_times_seq;

create table students (
  id                            bigint not null,
  is_teacher                    boolean,
  other_id                      varchar(255),
  name                          varchar(255),
  gender                        varchar(255),
  nights                        varchar(255),
  dietary                       varchar(255),
  comments                      varchar(255),
  release_form                  boolean,
  created_on                    timestamp,
  deactivated                   boolean,
  school_id                     bigint,
  constraint pk_students primary key (id)
);
create sequence students_seq;

create table students_sessions (
  students_id                   bigint not null,
  sessions_id                   bigint not null,
  constraint pk_students_sessions primary key (students_id,sessions_id)
);

create table teachers (
  id                            bigint not null,
  school_id                     bigint,
  constraint pk_teachers primary key (id)
);
create sequence teachers_seq;

create table token_action (
  id                            bigint not null,
  token                         varchar(255),
  target_user_id                bigint,
  type                          varchar(2),
  created                       timestamp,
  expires                       timestamp,
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id)
);
create sequence token_action_seq;

create table users (
  id                            bigint not null,
  email                         varchar(255),
  name                          varchar(255),
  first_name                    varchar(255),
  last_name                     varchar(255),
  last_login                    timestamp,
  active                        boolean,
  email_validated               boolean,
  constraint pk_users primary key (id)
);
create sequence users_seq;

create table users_security_role (
  users_id                      bigint not null,
  security_role_id              bigint not null,
  constraint pk_users_security_role primary key (users_id,security_role_id)
);

create table users_user_permission (
  users_id                      bigint not null,
  user_permission_id            bigint not null,
  constraint pk_users_user_permission primary key (users_id,user_permission_id)
);

create table user_permission (
  id                            bigint not null,
  value                         varchar(255),
  constraint pk_user_permission primary key (id)
);
create sequence user_permission_seq;

alter table linked_account add constraint fk_linked_account_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_id on linked_account (user_id);

alter table schools add constraint fk_schools_teacher_id foreign key (teacher_id) references users (id) on delete restrict on update restrict;

alter table sessions add constraint fk_sessions_timeslot_id foreign key (timeslot_id) references session_times (id) on delete restrict on update restrict;
create index ix_sessions_timeslot_id on sessions (timeslot_id);

alter table students add constraint fk_students_school_id foreign key (school_id) references schools (id) on delete restrict on update restrict;
create index ix_students_school_id on students (school_id);

alter table students_sessions add constraint fk_students_sessions_students foreign key (students_id) references students (id) on delete restrict on update restrict;
create index ix_students_sessions_students on students_sessions (students_id);

alter table students_sessions add constraint fk_students_sessions_sessions foreign key (sessions_id) references sessions (id) on delete restrict on update restrict;
create index ix_students_sessions_sessions on students_sessions (sessions_id);

alter table teachers add constraint fk_teachers_school_id foreign key (school_id) references schools (id) on delete restrict on update restrict;
create index ix_teachers_school_id on teachers (school_id);

alter table token_action add constraint fk_token_action_target_user_id foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_target_user_id on token_action (target_user_id);

alter table users_security_role add constraint fk_users_security_role_users foreign key (users_id) references users (id) on delete restrict on update restrict;
create index ix_users_security_role_users on users_security_role (users_id);

alter table users_security_role add constraint fk_users_security_role_security_role foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;
create index ix_users_security_role_security_role on users_security_role (security_role_id);

alter table users_user_permission add constraint fk_users_user_permission_users foreign key (users_id) references users (id) on delete restrict on update restrict;
create index ix_users_user_permission_users on users_user_permission (users_id);

alter table users_user_permission add constraint fk_users_user_permission_user_permission foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;
create index ix_users_user_permission_user_permission on users_user_permission (user_permission_id);


# --- !Downs

alter table linked_account drop constraint if exists fk_linked_account_user_id;
drop index if exists ix_linked_account_user_id;

alter table schools drop constraint if exists fk_schools_teacher_id;

alter table sessions drop constraint if exists fk_sessions_timeslot_id;
drop index if exists ix_sessions_timeslot_id;

alter table students drop constraint if exists fk_students_school_id;
drop index if exists ix_students_school_id;

alter table students_sessions drop constraint if exists fk_students_sessions_students;
drop index if exists ix_students_sessions_students;

alter table students_sessions drop constraint if exists fk_students_sessions_sessions;
drop index if exists ix_students_sessions_sessions;

alter table teachers drop constraint if exists fk_teachers_school_id;
drop index if exists ix_teachers_school_id;

alter table token_action drop constraint if exists fk_token_action_target_user_id;
drop index if exists ix_token_action_target_user_id;

alter table users_security_role drop constraint if exists fk_users_security_role_users;
drop index if exists ix_users_security_role_users;

alter table users_security_role drop constraint if exists fk_users_security_role_security_role;
drop index if exists ix_users_security_role_security_role;

alter table users_user_permission drop constraint if exists fk_users_user_permission_users;
drop index if exists ix_users_user_permission_users;

alter table users_user_permission drop constraint if exists fk_users_user_permission_user_permission;
drop index if exists ix_users_user_permission_user_permission;

drop table if exists linked_account;
drop sequence if exists linked_account_seq;

drop table if exists schools;
drop sequence if exists schools_seq;

drop table if exists security_role;
drop sequence if exists security_role_seq;

drop table if exists sessions;
drop sequence if exists sessions_seq;

drop table if exists session_times;
drop sequence if exists session_times_seq;

drop table if exists students;
drop sequence if exists students_seq;

drop table if exists students_sessions;

drop table if exists teachers;
drop sequence if exists teachers_seq;

drop table if exists token_action;
drop sequence if exists token_action_seq;

drop table if exists users;
drop sequence if exists users_seq;

drop table if exists users_security_role;

drop table if exists users_user_permission;

drop table if exists user_permission;
drop sequence if exists user_permission_seq;

