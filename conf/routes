# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

#
#   Main App Routing
#   ----------------

GET      /HSCBooster/                           controllers.Application.index

GET      /HSCBooster/signup                     controllers.Application.signup
POST     /HSCBooster/signup                     controllers.Application.doSignup

GET      /HSCBooster/profile                    controllers.Application.profile

GET      /HSCBooster/contact                    controllers.Info.contact
GET      /HSCBooster/about                      controllers.Info.about
GET      /HSCBooster/summaries                  controllers.Info.summaries

GET      /HSCBooster/school/new                 controllers.School.create
POST     /HSCBooster/school/new                 controllers.School.doCreate
GET      /HSCBooster/school/edit                controllers.School.update
POST     /HSCBooster/school/edit                controllers.School.doUpdate

GET      /HSCBooster/student/new                controllers.Student.create
GET      /HSCBooster/staff/new                  controllers.Student.createTeacher
POST     /HSCBooster/student/new                controllers.Student.doCreate
GET      /HSCBooster/students/:id/edit          controllers.Student.update(id: Long)
POST     /HSCBooster/students/:id/edit          controllers.Student.doUpdate(id: Long)
GET      /HSCBooster/students/:id/delete        controllers.Student.delete(id: Long)
POST     /HSCBooster/students/:id/delete        controllers.Student.doDelete(id: Long)
GET      /HSCBooster/students                   controllers.Students.index
POST     /HSCBooster/students/upload            controllers.Students.doUpload

GET      /HSCBooster/register                   controllers.Register.index

#
#   API
#   ---

GET      /HSCBooster/api/data                                       api.Register.data
POST     /HSCBooster/api/sessions/:id/students/:studentId/add       api.Register.add(id: Long, studentId: Long)
POST     /HSCBooster/api/sessions/:id/students/:studentId/remove    api.Register.remove(id: Long, studentId: Long)
POST     /HSCBooster/api/sessions/:id/capacity                      api.Register.capacity(id: Long)
GET      /HSCBooster/api/graph                                      api.Report.data
GET      /HSCBooster/api/schools                                    api.Report.schools
GET      /HSCBooster/api/students                                   api.Report.students

#
#   Admin
#   -----

GET      /HSCBooster/session/new                controllers.Session.create
POST     /HSCBooster/session/new                controllers.Session.doCreate
GET      /HSCBooster/session/:id/edit           controllers.Session.update(id: Long)
POST     /HSCBooster/session/:id/edit           controllers.Session.doUpdate(id: Long)
GET      /HSCBooster/session/:id/delete         controllers.Session.delete(id: Long)
POST     /HSCBooster/session/:id/delete         controllers.Session.doDelete(id: Long)
GET      /HSCBooster/sessions                   controllers.Sessions.index
POST     /HSCBooster/sessions/upload            controllers.Sessions.doUpload

GET      /HSCBooster/users                      controllers.Users.index

GET      /HSCBooster/report                     controllers.Report.dashboard

#
#   Play-Authenticate Routing
#   -----–-------------------

GET      /HSCBooster/login                      controllers.Application.login
POST     /HSCBooster/login                      controllers.Application.doLogin

GET      /HSCBooster/logout                     com.feth.play.module.pa.controllers.Authenticate.logout
GET      /HSCBooster/authenticate/:provider     com.feth.play.module.pa.controllers.Authenticate.authenticate(provider: String)

GET      /HSCBooster/accounts/unverified        controllers.Signup.unverified
GET      /HSCBooster/authenticate/:provider/denied     controllers.Signup.oAuthDenied(provider: String)

GET      /HSCBooster/accounts/verify/:token     controllers.Signup.verify(token: String)
GET      /HSCBooster/accounts/exists            controllers.Signup.exists

GET      /HSCBooster/accounts/password/reset/:token     controllers.Signup.resetPassword(token: String)
POST     /HSCBooster/accounts/password/reset            controllers.Signup.doResetPassword

GET      /HSCBooster/accounts/password/change    controllers.Account.changePassword
POST     /HSCBooster/accounts/password/change    controllers.Account.doChangePassword

GET      /HSCBooster/accounts/verify            controllers.Account.verifyEmail

GET      /HSCBooster/accounts/add               controllers.Account.link

GET      /HSCBooster/accounts/link              controllers.Account.askLink
POST     /HSCBooster/accounts/link              controllers.Account.doLink

GET      /HSCBooster/accounts/merge             controllers.Account.askMerge
POST     /HSCBooster/accounts/merge             controllers.Account.doMerge

GET      /HSCBooster/login/password/forgot     controllers.Signup.forgotPassword(email: String ?= "")
POST     /HSCBooster/login/password/forgot     controllers.Signup.doForgotPassword

#
#   Asset Routing
#   -------------

GET      /HSCBooster/assets/*file                   controllers.Assets.versioned(path="/public", file: Asset)
GET      /HSCBooster/assets/javascript/routes.js    controllers.Application.jsRoutes
GET      /HSCBooster/webjars/*file                  controllers.WebJarAssets.at(file)

# Duplicate config without the /HSCBooster url
# This should force Play to grab the above URI for output and use below URI for input...

GET / controllers.Application.index

GET      /signup                     controllers.Application.signup
POST     /signup                     controllers.Application.doSignup

GET      /profile                    controllers.Application.profile

GET      /contact                    controllers.Info.contact
GET      /about                      controllers.Info.about
GET      /summaries                  controllers.Info.summaries

GET      /school/new                 controllers.School.create
POST     /school/new                 controllers.School.doCreate
GET      /school/edit                controllers.School.update
POST     /school/edit                controllers.School.doUpdate

GET      /student/new                controllers.Student.create
GET      /staff/new                  controllers.Student.createTeacher
POST     /student/new                controllers.Student.doCreate
GET      /students/:id/edit          controllers.Student.update(id: Long)
POST     /students/:id/edit          controllers.Student.doUpdate(id: Long)
GET      /students/:id/delete        controllers.Student.delete(id: Long)
POST     /students/:id/delete        controllers.Student.doDelete(id: Long)
GET      /students                   controllers.Students.index
POST     /students/upload            controllers.Students.doUpload

GET      /register                   controllers.Register.index

#
#   API
#   ---

GET      /api/data                                       api.Register.data
POST     /api/sessions/:id/students/:studentId/add       api.Register.add(id: Long, studentId: Long)
POST     /api/sessions/:id/students/:studentId/remove    api.Register.remove(id: Long, studentId: Long)
POST     /api/sessions/:id/capacity                      api.Register.capacity(id: Long)
GET      /api/graph                                      api.Report.data
GET      /api/schools                                    api.Report.schools
GET      /api/students                                   api.Report.students

#
#   Admin
#   -----

GET      /session/new                controllers.Session.create
POST     /session/new                controllers.Session.doCreate
GET      /session/:id/edit           controllers.Session.update(id: Long)
POST     /session/:id/edit           controllers.Session.doUpdate(id: Long)
GET      /session/:id/delete         controllers.Session.delete(id: Long)
POST     /session/:id/delete         controllers.Session.doDelete(id: Long)
GET      /sessions                   controllers.Sessions.index
POST     /sessions/upload            controllers.Sessions.doUpload

GET      /users                      controllers.Users.index

GET      /report                     controllers.Report.dashboard

#
#   Play-Authenticate Routing
#   -----–-------------------

GET      /login                      controllers.Application.login
POST     /login                      controllers.Application.doLogin

GET      /logout                     com.feth.play.module.pa.controllers.Authenticate.logout
GET      /authenticate/:provider     com.feth.play.module.pa.controllers.Authenticate.authenticate(provider: String)

GET      /accounts/unverified        controllers.Signup.unverified
GET      /authenticate/:provider/denied     controllers.Signup.oAuthDenied(provider: String)

GET      /accounts/verify/:token     controllers.Signup.verify(token: String)
GET      /accounts/exists            controllers.Signup.exists

GET      /accounts/password/reset/:token     controllers.Signup.resetPassword(token: String)
POST     /accounts/password/reset            controllers.Signup.doResetPassword

GET      /accounts/password/change    controllers.Account.changePassword
POST     /accounts/password/change    controllers.Account.doChangePassword

GET      /accounts/verify            controllers.Account.verifyEmail

GET      /accounts/add               controllers.Account.link

GET      /accounts/link              controllers.Account.askLink
POST     /accounts/link              controllers.Account.doLink

GET      /accounts/merge             controllers.Account.askMerge
POST     /accounts/merge             controllers.Account.doMerge

GET      /login/password/forgot     controllers.Signup.forgotPassword(email: String ?= "")
POST     /login/password/forgot     controllers.Signup.doForgotPassword

#
#   Asset Routing
#   -------------

GET      /assets/*file                   controllers.Assets.versioned(path="/public", file: Asset)
GET      /assets/javascript/routes.js    controllers.Application.jsRoutes
GET      /webjars/*file                  controllers.WebJarAssets.at(file)