package security;

import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import be.objectify.deadbolt.java.ExecutionContextProvider;
import be.objectify.deadbolt.java.models.Subject;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUserIdentity;
import controllers.Application;
import controllers.routes;
import models.User;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Result;
import service.UserService;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.flash;

public class MyDeadboltHandler extends AbstractDeadboltHandler {

	private final PlayAuthenticate auth;
	private final service.UserService userService;

	public MyDeadboltHandler(final PlayAuthenticate auth, final ExecutionContextProvider exContextProvider,
							 final service.UserService userService) {
		super(exContextProvider);
		this.auth = auth;
		this.userService = userService;
	}

	@Override
	public CompletionStage<Optional<Result>> beforeAuthCheck(final Http.Context context) {
		if (this.auth.isLoggedIn(context.session())) {
			// user is logged in

			return CompletableFuture.completedFuture(Optional.empty());
		} else {
			// user is not logged in
			context.session().clear();
			// call this if you want to redirect your visitor to the page that
			// was requested before sending him to the login page
			// if you don't call this, the user will get redirected to the page
			// defined by your resolver
			final String originalUrl = this.auth.storeOriginalUrl(context);

			context.flash().put("error", Messages.get("playauthenticate.handler.loginfirst"));
			return CompletableFuture.completedFuture(Optional.ofNullable(redirect(this.auth.getResolver().login())));
		}
	}

	@Override
	public CompletionStage<Optional<? extends Subject>> getSubject(final Http.Context context) {
		final AuthUserIdentity u = this.auth.getUser(context);
		// Caching might be a good idea here
		return CompletableFuture.completedFuture(Optional.ofNullable((Subject)userService.findByAuthUserIdentity(u)));
	}

	@Override
	public CompletionStage<Optional<DynamicResourceHandler>> getDynamicResourceHandler(
			final Http.Context context) {
		return CompletableFuture.completedFuture(Optional.empty());
	}

	@Override
	public CompletionStage<Result> onAuthFailure(final Http.Context context,
												 final Optional<String> content) {
		// if the user has a cookie with a valid user and the local user has
		// been deactivated/deleted in between, it is possible that this gets
		// shown. You might want to consider to sign the user out in this case.
		flash(Application.FLASH_ERROR_KEY, "Unable to access the requested page");
        return CompletableFuture.completedFuture(redirect(controllers.routes.Application.index()));
	}

}
