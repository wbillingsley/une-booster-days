package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import play.mvc.Controller;
import play.mvc.Result;
import service.SchoolService;
import service.SessionService;
import service.UserService;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
//import java.util.Date;
//import java.text.SimpleDateFormat;

public class Register extends Controller {

    private final UserService userService;
    private final SchoolService schoolService;
    private final SessionService sessionService;
    private final PlayAuthenticate auth;

    @Inject
    public Register(final UserService userService, final SchoolService schoolService,
                    final SessionService sessionService, final PlayAuthenticate auth) {
        this.userService = userService;
        this.schoolService = schoolService;
        this.sessionService = sessionService;
        this.auth = auth;
    }

    /* Index */
    @Restrict(@Group(Application.USER_ROLE))
    public Result index() {
        AuthUser authUser = auth.getUser(session());
        models.School school = schoolService.findByAuthUserIdentity(authUser);
        if (school == null) {
            flash(Application.FLASH_ERROR_KEY, "School has not been registered yet");
            return redirect(controllers.routes.School.create());
        }
        Date stopDate = null;
        try {
            stopDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm")).parse("2019-05-28 08:00");
            Date currDate = new Date();
            if(true || currDate.after(stopDate)){
                flash(Application.FLASH_ERROR_KEY,"Cut-off for registrations has closed. You will still be able to see your registrations, but your changes will NOT be saved.");
            }
        } catch (ParseException e) {
            //Eh
        }
        List<models.Student> students = school.students.stream()
                .filter(x->!x.isTeacher)
                .collect(Collectors.toList());
        List<models.Session> sessions = sessionService.getAll();
        return ok(views.html.registerStudents.render(userService, sessions, students));
    }

}

