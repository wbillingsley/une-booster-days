package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import models.*;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthProvider.MyLogin;
import providers.MyUsernamePasswordAuthProvider.MySignup;
import service.SessionService;
import service.UserService;
import views.html.*;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Main Application Controller
 */
public class Application extends Controller {

	/* Flash Types */

	public static final String FLASH_MESSAGE_KEY = "message";
	public static final String FLASH_ERROR_KEY = "error";

	/* Role Types */

	public static final String ADMIN_ROLE = "admin";
	public static final String USER_ROLE = "user";
	public static final String REPORT_ROLE = "report";

	/* Injected */

	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserService userService;

    private final SessionService sessionService;

	/* Formatting */

	public static String formatTimestamp(final long t) {
		return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
	}

	@Inject
	public Application(final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
                       final UserService userService, final SessionService sessionService) {
		this.auth = auth;
		this.provider = provider;
		this.userService = userService;
        this.sessionService = sessionService;
    }

	/* Endpoints */

	/**
	 * GET: Main Page
	 * @return
	 */
	public Result index() {
        List<models.Session> sessions = sessionService.getAll();
		return ok(index.render(this.userService, sessions));
	}

	/**
	 * GET: User Profile
	 * @return
	 */
	@Restrict(@Group(Application.USER_ROLE))
	public Result profile() {
		final User localUser = userService.getUser(session());
		return ok(profile.render(this.auth, this.userService, localUser));
	}

	/**
	 * GET: Login
	 * @return
	 */
	public Result login() {
		return ok(login.render(this.auth, this.userService,  this.provider.getLoginForm()));
	}

	public Result doLogin() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MyLogin> filledForm = this.provider.getLoginForm()
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(login.render(this.auth, this.userService, filledForm));
		} else {
			// Everything was filled
			return this.provider.handleLogin(ctx());
		}
	}

	/**
	 * GET: Signup
	 * @return
	 */
	public Result signup() {
		boolean regoDisabled = false; // TODO: External config
		if (regoDisabled) {
			flash(Application.FLASH_ERROR_KEY, "Registration for the 2019 booster days will open on April 3rd. Please contact Sally Strelitz on 02 6773 1249 or sally.strelitz@une.edu.au.");
		}
		return ok(signup.render(this.auth, this.userService, this.provider.getSignupForm()));
	}

	/**
	 * POST: Signup
	 * @return
	 */
	public Result doSignup() {
		boolean regoDisabled = false; // TODO: External config
		if (regoDisabled) {
			return redirect(routes.Application.signup());
		}
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MySignup> filledForm = this.provider.getSignupForm().bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(signup.render(this.auth, this.userService, filledForm));
		} else {
			return this.provider.handleSignup(ctx());
		}
	}

	/**
	 * GET: Routes
	 * @return
	 */
	public Result jsRoutes() {
		return ok(
				play.routing.JavaScriptReverseRouter.create("jsRoutes",
						routes.javascript.Signup.forgotPassword()))
				.as("text/javascript");

	}

}
