package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import controllers.*;
import models.User;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.SchoolService;
import service.StudentService;
import service.UserService;

import javax.inject.Inject;

public class School extends Controller {

    private final UserService userService;
    private final FormFactory formFactory;
    private final StudentService studentService;
    private final SchoolService schoolService;
    private final PlayAuthenticate auth;

    @Inject
    public School(final UserService userService, final FormFactory formFactory,
                  final StudentService studentService,
                  final SchoolService schoolService, final PlayAuthenticate auth) {
        this.userService = userService;
        this.formFactory = formFactory;
        this.studentService = studentService;
        this.schoolService = schoolService;
        this.auth = auth;
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result create() {
        final AuthUser authUser = auth.getUser(session());
        final boolean exists = schoolService.existsByAuthUserIdentity(authUser);
        if (exists) {
            return redirect(controllers.routes.School.update());
        }
        final Form<SchoolService.SchoolForm> schoolForm = formFactory.form(SchoolService.SchoolForm.class);
        return ok(views.html.school.render(this.userService, schoolForm, controllers.routes.School.doCreate()));
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doCreate() {
        final AuthUser authUser = auth.getUser(session());
        final boolean exists = schoolService.existsByAuthUserIdentity(authUser);
        if (exists) {
            flash(Application.FLASH_ERROR_KEY, "School has already been created");
            return redirect(controllers.routes.Application.index());
        }
        final Form<SchoolService.SchoolForm> filledForm = this.formFactory.form(SchoolService.SchoolForm.class).bindFromRequest();
        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submitted");
            return badRequest(views.html.school.render(userService, filledForm, controllers.routes.School.doCreate()));
        } else {
            schoolService.create(authUser, filledForm.get());
            models.School school = schoolService.findByAuthUserIdentity(authUser);
            User user = userService.findByAuthUserIdentity(authUser);
            StudentService.StudentForm studentForm = new StudentService.StudentForm();
            studentForm.schoolId = "TEACHER";
            studentForm.name = user.name;
            studentForm.isTeacher = true;
            Long studentId = studentService.createForSchool(user, school, studentForm);
            Logger.info("School {} / Teacher {} created", school.id, studentId);
            flash(Application.FLASH_MESSAGE_KEY, "School details updated");
            return redirect(controllers.routes.Application.index());
        }
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result update() {
        final AuthUser authUser = auth.getUser(session());
        final models.School school = schoolService.findByAuthUserIdentity(authUser);
        if (school == null) return redirect(controllers.routes.School.create());
        final Form<SchoolService.SchoolForm> filledForm = formFactory.form(SchoolService.SchoolForm.class)
                .fill(new SchoolService.SchoolForm(school));
        return ok(views.html.school.render(this.userService, filledForm, controllers.routes.School.doUpdate()));
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doUpdate() {
        final AuthUser authUser = auth.getUser(session());
        final models.School school = schoolService.findByAuthUserIdentity(authUser);
        if (school == null) {
            return redirect(controllers.routes.School.create());
        }
        final Form<SchoolService.SchoolForm> filledForm = this.formFactory.form(SchoolService.SchoolForm.class).bindFromRequest();
        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submitted");
            return badRequest(views.html.school.render(userService, filledForm, controllers.routes.School.doUpdate()));
        } else {
            schoolService.update(authUser, filledForm.get());
            Logger.info("School {} updated", school.id);
            flash(Application.FLASH_MESSAGE_KEY, "School details updated");
            return redirect(controllers.routes.Application.index());
        }
    }

}
