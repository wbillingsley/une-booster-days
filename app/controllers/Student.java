package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.SchoolService;
import service.StudentService;
import service.UserService;

import javax.inject.Inject;

/**
 * Created by jnagy on 23/3/17.
 */
public class Student extends Controller {

    private final UserService userService;
    private final PlayAuthenticate auth;
    private final FormFactory formFactory;
    private final StudentService studentService;
    private final SchoolService schoolService;

    @Inject
    public Student(final UserService userService, final PlayAuthenticate auth,
                   final FormFactory formFactory, final StudentService studentService,
                   final SchoolService schoolService) {

        this.auth = auth;
        this.formFactory = formFactory;

        this.userService = userService;
        this.studentService = studentService;
        this.schoolService = schoolService;
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result create() {

        final Form<StudentService.StudentForm> studentForm = formFactory.form(StudentService.StudentForm.class);
        final Form<StudentService.StudentForm> defaultForm = studentForm.fill(new StudentService.StudentForm());
        return ok(views.html.createStudent
                .render(this.userService, defaultForm, controllers.routes.Student.doCreate(), false));

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result createTeacher() {
        final Form<StudentService.StudentForm> studentForm = formFactory.form(StudentService.StudentForm.class);
        final StudentService.StudentForm sFrm = new StudentService.StudentForm();
        sFrm.isTeacher = true;
        sFrm.releaseForm = true;
        final Form<StudentService.StudentForm> defaultForm = studentForm.fill(sFrm);
        return ok(views.html.createStudent
                .render(this.userService, defaultForm, controllers.routes.Student.doCreate(), true));

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doCreate() {
        final Form<StudentService.StudentForm> postedForm = formFactory.form(StudentService.StudentForm.class);
        final Form<StudentService.StudentForm> filledForm = postedForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submission");
            //We can rely on isTeacher since the form never alters it even if it is a "bad" form
            return badRequest(views.html.createStudent
                    .render(this.userService, filledForm, controllers.routes.Student.doCreate(), filledForm.get().isTeacher));
        }
        AuthUser authUser = auth.getUser(session());
        models.School school = schoolService.findByAuthUserIdentity(authUser);
        models.User user = userService.getUser(session());
        StudentService.StudentForm formData = filledForm.get();

        Long id = studentService.createForSchool(user, school, formData);
        if(formData.isTeacher){
            Logger.info("Staff Member {} added", id);
            flash(Application.FLASH_MESSAGE_KEY, "Staff Member added");
        }else{
            Logger.info("Student {} added", id);
            flash(Application.FLASH_MESSAGE_KEY, "Student added");
        }
        return redirect(controllers.routes.Students.index());
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result update(long id) {

        final AuthUser authUser = auth.getUser(session());
        final models.Student student = studentService.findForAuthUserIdentity(id, authUser);
        if (student == null)
            return studentNotFound();

        final Form<StudentService.StudentForm> studentForm = formFactory.form(StudentService.StudentForm.class);
        final Form<StudentService.StudentForm> filledForm = studentForm.fill(new StudentService.StudentForm(student));

        return ok(views.html.createStudent.render(userService, filledForm, routes.Student.doUpdate(id), student.isTeacher));

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doUpdate(long id) {

        final AuthUser authUser = auth.getUser(session());
        final models.Student student = studentService.findForAuthUserIdentity(id, authUser);
        if (student == null)
            return studentNotFound();

        final Form<StudentService.StudentForm> studentForm = formFactory.form(StudentService.StudentForm.class);
        final Form<StudentService.StudentForm> filledForm = studentForm.bindFromRequest();

        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submitted");
            return badRequest(views.html.createStudent.render(userService, filledForm, routes.Student.doUpdate(id), student.isTeacher));
        } else {
            StudentService.StudentForm formData = filledForm.get();
            Long updateid = studentService.updateStudent(authUser, id, formData);
            if (updateid == null) {
                flash(Application.FLASH_ERROR_KEY, "Could not update student record.");
                return badRequest(views.html.createStudent.render(userService, filledForm, routes.Student.doUpdate(id), student.isTeacher));
            }
            Logger.info("Student {} updated", id);
            flash(Application.FLASH_MESSAGE_KEY, "Student details updated");
            return redirect(routes.Students.index());
        }

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result delete(long id) {
        final AuthUser authUser = auth.getUser(session());
        final models.Student student = studentService.findForAuthUserIdentity(id, authUser);
        if (student == null)
            return studentNotFound();
        if (student.isTeacher)
            return cannotDeleteTeacher();
        return ok(views.html.deleteStudent.render(userService, student));
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doDelete(final long id) {
        final AuthUser authUser = auth.getUser(session());
        final models.Student student = studentService.findForAuthUserIdentity(id, authUser);
        if (student == null)
            return studentNotFound();
        if (student.isTeacher)
            return cannotDeleteTeacher();
        studentService.delete(student);
        Logger.info("Student {} deleted", id);
        return redirect(controllers.routes.Students.index());
    }

    private Result studentNotFound() {
        flash(Application.FLASH_ERROR_KEY, "Student not found");
        return redirect(controllers.routes.Students.index());
    }

    private Result cannotDeleteTeacher() {
        flash(Application.FLASH_ERROR_KEY, "You can not delete your own registration.");
        return redirect(controllers.routes.Students.index());
    }

}
