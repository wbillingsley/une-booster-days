package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.SessionService;
import service.UserService;

import javax.inject.Inject;

/**
 * Created by jnagy on 24/3/17.
 */
public class Session extends Controller {

    private final UserService userService;
    private final PlayAuthenticate auth;
    private final FormFactory formFactory;
    private final SessionService sessionService;

    @Inject
    public Session(final UserService userService, final PlayAuthenticate auth,
                   final FormFactory formFactory, final SessionService sessionService) {

        this.userService = userService;
        this.auth = auth;
        this.formFactory = formFactory;
        this.sessionService = sessionService;
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result get(Long id) {
        return TODO;
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result create() {

        Form<SessionService.SessionForm> form = formFactory.form(SessionService.SessionForm.class);
        return ok(views.html.createSession.render(userService, form, routes.Session.doCreate()));

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result doCreate() {

        Form<SessionService.SessionForm> postedForm = formFactory.form(SessionService.SessionForm.class);
        Form<SessionService.SessionForm> filledForm = postedForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submission");
            return badRequest(views.html.createSession.render(userService, filledForm, routes.Session.doCreate()));
        }

        AuthUser authUser = auth.getUser(session());
        SessionService.SessionForm formData = filledForm.get();
        sessionService.create(authUser, formData);
        flash(Application.FLASH_MESSAGE_KEY, "Session added");
        return redirect(routes.Sessions.index());

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result delete(Long id) {

        models.Session session = sessionService.findById(id);
        if (session.students.size() > 0) {
            flash(Application.FLASH_ERROR_KEY, "Cannot delete session with allocated students");
            return redirect(routes.Sessions.index());
        }
        return ok(views.html.deleteSession.render(userService, session));

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result doDelete(Long id) {

        models.Session session = sessionService.findById(id);
        if (session == null)
            return sessionNotFound(id);

        if (session.students.size() > 0) {
            flash(Application.FLASH_ERROR_KEY, "Cannot delete session with allocated students");
            return redirect(routes.Sessions.index());
        }

        sessionService.deleteById(id);
        return redirect(routes.Sessions.index());

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result update(Long id) {

        models.Session session = sessionService.findById(id);
        if (session == null)
            return sessionNotFound(id);

        Form<SessionService.SessionForm> defaultForm = formFactory.form(SessionService.SessionForm.class);
        Form<SessionService.SessionForm> formDisplay = defaultForm.fill(new SessionService.SessionForm(session));

        return ok(views.html.createSession.render(userService, formDisplay, routes.Session.doUpdate(id)));

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result doUpdate(Long id) {

        models.Session session = sessionService.findById(id);
        if (session == null)
            return sessionNotFound(id);

        // TODO: Check capacity...

        Form<SessionService.SessionForm> postedForm = formFactory.form(SessionService.SessionForm.class);
        Form<SessionService.SessionForm> filledForm = postedForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            flash(Application.FLASH_ERROR_KEY, "Invalid form submission");
            return badRequest(views.html.createSession.render(userService, filledForm, routes.Session.update(id)));
        }

        SessionService.SessionForm formData = filledForm.get();
        AuthUser authUser = auth.getUser(session());
        User user = userService.findByAuthUserIdentity(authUser);
        sessionService.updateSession(user, id, formData);

        flash(Application.FLASH_MESSAGE_KEY, "Session updated");

        return redirect(routes.Sessions.index());

    }

    private Result sessionNotFound(Long id) {
        flash(Application.FLASH_ERROR_KEY, "Session not found");
        return redirect(routes.Sessions.index());
    }

}
