package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import models.*;
import models.School;
import models.Student;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.ExcelService;
import service.SchoolService;
import service.StudentService;
import service.UserService;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Students extends Controller {

    private final UserService userService;
    private final SchoolService schoolService;
    private final PlayAuthenticate auth;
    private final StudentService studentService;

    @Inject
    public Students(final UserService userService, final PlayAuthenticate auth,
                    final SchoolService schoolService, final StudentService studentService) {

        this.userService = userService;
        this.auth = auth;
        this.schoolService = schoolService;
        this.studentService = studentService;

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result index() {
        final AuthUser authUser = auth.getUser(session());
        final models.School school = schoolService.findByAuthUserIdentity(authUser);
        if (school == null) {
            flash(Application.FLASH_ERROR_KEY, School.SCHOOL_DETAILS_EMPTY);
            return redirect(controllers.routes.School.create());
        }
        List<models.Student> students = school.students;
        students.sort((a,b)->a.name.compareTo(b.name));
        students.sort((a,b)->a.id.compareTo(b.id));
        students.sort((a,b) -> a.isTeacher.compareTo(b.isTeacher));
        students.stream()
                .forEach(student -> student.sessions.sort(
                        (a,b) -> a.timeslot.code.compareTo(b.timeslot.code)
                )
        );
        return ok(views.html.students.render(userService, students));

    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result doUpload() {

        final AuthUser authUser = auth.getUser(session());
        models.School school = schoolService.findByAuthUserIdentity(authUser);
        if (school == null) {
            flash(Application.FLASH_ERROR_KEY, "School has not been registered yet");
            return redirect(controllers.routes.School.create());
        }

        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> excel = body.getFile("excel");
        if (excel == null) {
            flash(Application.FLASH_ERROR_KEY, "Missing file");
            return redirect(controllers.routes.Students.index());
        }

        // TODO: check file extension and content type

        int size = school.students.size();

        File file = excel.getFile();
        List<StudentService.StudentForm> studentForms;

        try {
            studentForms = ExcelService.readStudentNames(file);
        } catch (Exception e) {
            e.printStackTrace();
            flash(Application.FLASH_ERROR_KEY, "Failed to read file: " + e.getMessage());
            return redirect(controllers.routes.Students.index());
        }

        int notUploaded = 0;
        int numCreated = 0;
        User user = userService.findByAuthUserIdentity(authUser);
        for (StudentService.StudentForm studentForm : studentForms) {
            Long id = studentService.createForSchool(user, school, studentForm);
            if (id == null) {
                notUploaded++;
            } else {
                numCreated++;
                size++;
            }
            if (size >= 200) break;
        }

        List<NightCount> nightCounts = studentService.getNightCounts();

        String obNights = "";
        if (nightCounts.get(0).c > 180) {
            obNights += "1";
        }
        if (nightCounts.get(1).c > 180){
            if (obNights.length() > 0) obNights += " and ";
            obNights += "2";
        }

        if (obNights.length() > 0) {
            flash(Application.FLASH_ERROR_KEY, "Some students may not have been added.  Overnight accommodation limit has been reached for night(s) " + obNights + ".  Please contact us for assistance.");
        } else if (size >= 200) {
            flash(Application.FLASH_ERROR_KEY, "Maximum number of students reached (200).  Please contact us for assistance.");
        } else if (notUploaded > 0) {
            flash(Application.FLASH_ERROR_KEY, String.valueOf(notUploaded) + " duplicate students were found in the list");
        }

        flash(Application.FLASH_MESSAGE_KEY, String.valueOf(numCreated) + " new students uploaded.");

        return redirect(controllers.routes.Students.index());

    }

}
