package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserService;

import javax.inject.Inject;

public class Report extends Controller {

    private final UserService userService;

    @Inject
    public Report(final UserService userService) {
        this.userService = userService;
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result dashboard() {

        return ok(views.html.dashboard.render(userService));

    }
}