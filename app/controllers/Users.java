package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserService;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jnagy on 6/4/17.
 */
public class Users extends Controller {

    private final UserService userService;

    @Inject
    public Users(UserService userService) {
        this.userService = userService;
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result index() {

        List<User> users = userService.getAll();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date cutoff = c.getTime();

        Map<Boolean, List<User>> groups = users.stream()
                .collect(Collectors.groupingBy(user -> user.lastLogin.after(cutoff)));
        if(groups == null){
            groups = new HashMap<>();
        }
        groups.computeIfAbsent(true, k -> new LinkedList<>());
        groups.computeIfAbsent(false, k -> new LinkedList<>());
        return ok(views.html.users.render(userService, groups));

    }

}
