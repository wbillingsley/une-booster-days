package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import models.User;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.ExcelService;
import service.SchoolService;
import service.SessionService;
import service.UserService;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jnagy on 23/3/17.
 */
public class Sessions extends Controller {

    private final PlayAuthenticate auth;
    private final SchoolService schoolService;
    private final SessionService sessionService;
    private final UserService userService;

    @Inject
    public Sessions(final PlayAuthenticate auth, final SchoolService schoolService,
                    final SessionService sessionService, final UserService userService) {
        this.auth = auth;
        this.schoolService = schoolService;
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result index() {
        List<models.Session> sessions = sessionService.getAll();
        return ok(views.html.sessions.render(userService, sessions));
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result doUpload() {

        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> excel = body.getFile("excel");
        if (excel == null) {
            flash(Application.FLASH_ERROR_KEY, "Missing file");
            return redirect(controllers.routes.Sessions.index());
        }

        File file = excel.getFile();

        List<SessionService.SessionForm> sessionData;

        try {
            sessionData = ExcelService.readSessionData(file);
        } catch (Exception e) {
            e.printStackTrace();
            flash(Application.FLASH_ERROR_KEY, "Failed to read file: " + e.getMessage());
            return redirect(controllers.routes.Sessions.index());
        }

        int numFailed = 0;
        int numCreated = 0;
        AuthUser authUser = auth.getUser(session());

        for (SessionService.SessionForm session : sessionData) {
            Long id = sessionService.create(authUser, session);
            if (id == null) numFailed++;
            else numCreated++;
        }

        if (numFailed > 0) flash(Application.FLASH_ERROR_KEY, String.valueOf(numFailed) + " sessions failed to upload");
        flash(Application.FLASH_MESSAGE_KEY, String.valueOf(numCreated) + " new sessions uploaded.");

        return redirect(controllers.routes.Sessions.index());
    }

}
