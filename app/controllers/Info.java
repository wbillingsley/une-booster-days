package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import service.UserService;

import javax.inject.Inject;

/**
 * Info Controller
 */
public class Info extends Controller {

    private final UserService userService;

    @Inject
    public Info(final UserService userService) {
        this.userService = userService;
    }

    public Result about() {

        return ok(views.html.about.render(userService));

    }

    public Result contact() {

        return ok(views.html.contactUs.render(userService));

    }

    public Result summaries() {
        return ok(views.html.summaries.render(userService));
    }

}
