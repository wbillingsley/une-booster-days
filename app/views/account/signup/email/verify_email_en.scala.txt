@(verificationUrl: String, token: String, name: String, email: String)

Dear @name,

You recently signed up for the UNE HSC Booster Days 2018 Registration.

To activate your account, follow this link now:
@verificationUrl

Regards,
The UNE HSC Booster Days Team