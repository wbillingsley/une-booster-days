package handlers;

import play.http.HttpErrorHandler;
import play.mvc.*;
import play.mvc.Http.*;
import service.UserService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ErrorHandler implements HttpErrorHandler {

    private final UserService userService;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    public ErrorHandler(final UserService userService) {
        this.userService = userService;
    }

    public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {

        return CompletableFuture.completedFuture(
                Results.status(statusCode, views.html._error.render(userService, message))
        );
    }

    public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        logger.severe(exception.getMessage());
        exception.printStackTrace();
        return CompletableFuture.completedFuture(
                Results.internalServerError(views.html._error.render(userService, "Internal server error."))
        );
    }
}