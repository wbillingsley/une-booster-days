package service;

import com.avaje.ebean.Model;
import com.feth.play.module.pa.user.AuthUser;
import models.AppModel;
import models.School;
import models.User;
import play.data.validation.Constraints;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Service Layer for School DB Entity
 */
public class SchoolService {

    private final UserService userService;

    /**
     * Form for School create and edit
     */
    public static class SchoolForm {

        @Constraints.Required
        public String name;

        @Constraints.Required
        public String streetAddress;

        @Constraints.Required
        public String townSuburb;

        @Constraints.Required
        @Constraints.Pattern(value = "\\d{4}", message = "Valid post code required")
        public String postCode;

        @Constraints.Required
        public String state;

        public SchoolForm() {

        }

        public SchoolForm(School school) {
            this.name = school.name;
            this.streetAddress = school.streetAddress;
            this.townSuburb = school.townSuburb;
            this.postCode = school.postCode;
            this.state = school.state;
        }
    }

    public static final AppModel.Finder<Long, School> find = new Model.Finder<>(Long.class, School.class);

    @Inject
    public SchoolService(final UserService userService) {
        this.userService = userService;
    }

    @Nullable
    public Long create(final AuthUser authUser, final SchoolForm schoolForm) {

        final User user = userService.findByAuthUserIdentity(authUser);
        if (user == null) return null;

        final School school = new School(
                schoolForm.name,
                schoolForm.streetAddress,
                schoolForm.townSuburb,
                schoolForm.state,
                schoolForm.postCode,
                user
        );

        school.save();

        return school.id;

    }

    /**
     * Update an existing school record associated with the current AuthUser
     * @param authUser
     * @param schoolForm
     * @return
     */
    @Nullable
    public Long update(AuthUser authUser, SchoolForm schoolForm) {

        // TODO: Also allow non-primary teachers to edit?

        final School school = findByAuthUserIdentity(authUser);
        if (school == null) return null;

        final User user = userService.findByAuthUserIdentity(authUser);

        school.name = schoolForm.name;
        school.streetAddress = schoolForm.streetAddress;
        school.townSuburb = schoolForm.townSuburb;
        school.state = schoolForm.state;
        school.postCode = schoolForm.postCode;

        school.modifiedBy = user;
        school.modifiedOn = new Date();

        school.save();

        return school.id;
    }

    /**
     * Find a school entity for the given AuthUser
     * @param authUser current AuthUser
     * @return School entity or null
     */
    @Nullable
    public School findByAuthUserIdentity(AuthUser authUser) {

        final User user = userService.findByAuthUserIdentity(authUser);

        // TODO: Also find by colleague property

        return find.where()
                .eq("teacher.id", user.id)
                  .filterMany("students")
                  .ne("deactivated", true)
                .findUnique();

    }

    public List<School> getAll() {
        return find.where()
                .ne("deactivated", true)
                    .filterMany("students")
                    .ne("deactivated", true)
                .findList();
    }

    /**
     * Implies user can only relate to a single school
     * @param authUser
     * @return
     */
    public boolean existsByAuthUserIdentity(final AuthUser authUser) {

        final User user = userService.findByAuthUserIdentity(authUser);

        return find.where()
                .eq("teacher.id", user.id)
                /* Note: This teacher join thing never panned out, there is
                   no join teacher record created so there is no join
                 */
                .findRowCount() > 0;

    }

}
