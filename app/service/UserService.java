package service;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Model;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.*;
import controllers.Application;
import models.*;
import org.jetbrains.annotations.Nullable;
import play.mvc.Http.Session;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Service layer for User DB entity
 */
public class UserService {

    private final PlayAuthenticate auth;

    private static final AppModel.Finder<Long, User> find = new Model.Finder<>(
            Long.class, User.class);

    @Inject
    public UserService(final PlayAuthenticate auth) {
        this.auth = auth;
    }

    @Nullable
    public User getUser(Session session) {
        final AuthUser currentAuthUser = this.auth.getUser(session);
        final User localUser = findByAuthUserIdentity(currentAuthUser);
        return localUser;
    }

    public boolean existsByAuthUserIdentity(
            final AuthUserIdentity identity) {
        final ExpressionList<User> exp;
        if (identity instanceof UsernamePasswordAuthUser) {
            exp = getUsernamePasswordAuthUserFind((UsernamePasswordAuthUser) identity);
        } else {
            exp = getAuthUserFind(identity);
        }
        return exp.findRowCount() > 0;
    }

    public List<User> getAll() {
        return find.all();
    }

    public User findByAuthUserIdentity(final AuthUserIdentity identity) {
        if (identity == null) {
            return null;
        }
        if (identity instanceof UsernamePasswordAuthUser) {
            return findByUsernamePasswordIdentity((UsernamePasswordAuthUser) identity);
        } else {
            return getAuthUserFind(identity).findUnique();
        }
    }

    public User findByUsernamePasswordIdentity(
            final UsernamePasswordAuthUser identity) {
        return getUsernamePasswordAuthUserFind(identity).findUnique();
    }

    public User create(final AuthUser authUser) {

        final User user = new User();
        user.roles = new ArrayList<>();
        user.roles.add(SecurityRole.findByRoleName(controllers.Application.USER_ROLE));
        if (find.findRowCount() == 0)
            user.roles.add(SecurityRole.findByRoleName(Application.ADMIN_ROLE));
        user.active = true;
        user.lastLogin = new Date();
        user.linkedAccounts = Collections.singletonList(LinkedAccount.create(authUser));

        if (authUser instanceof EmailIdentity) {
            final EmailIdentity identity = (EmailIdentity) authUser;
            user.email = identity.getEmail();
            user.emailValidated = true ;
        }

        if (authUser instanceof NameIdentity) {
            final NameIdentity identity = (NameIdentity) authUser;
            final String name = identity.getName();
            if (name != null) {
                user.name = name;
            }
        }

        if (authUser instanceof FirstLastNameIdentity) {
            final FirstLastNameIdentity identity = (FirstLastNameIdentity) authUser;
            final String firstName = identity.getFirstName();
            final String lastName = identity.getLastName();
            if (firstName != null) {
                user.firstName = firstName;
            }
            if (lastName != null) {
                user.lastName = lastName;
            }
        }

        user.save();
        // Ebean.saveManyToManyAssociations(user, "roles");
        // Ebean.saveManyToManyAssociations(user, "permissions");
        return user;
    }

    public void merge(final AuthUser oldUser, final AuthUser newUser) {
        findByAuthUserIdentity(oldUser).merge(
                findByAuthUserIdentity(newUser));
    }

    public void addLinkedAccount(final AuthUser oldUser,
                                        final AuthUser newUser) {
        final User u = findByAuthUserIdentity(oldUser);
        u.linkedAccounts.add(LinkedAccount.create(newUser));
        u.save();
    }

    public void setLastLoginDate(final AuthUser knownUser) {
        final User u = findByAuthUserIdentity(knownUser);
        u.lastLogin = new Date();
        u.save();
    }

    public User findByEmail(final String email) {
        return getEmailUserFind(email).findUnique();
    }

    public void verify(final User unverified) {
        // You might want to wrap this into a transaction
        System.out.println("User is verified? " + String.valueOf(unverified.emailValidated));
        unverified.emailValidated = true;
        unverified.save();

        TokenAction.deleteByUser(unverified, TokenAction.Type.EMAIL_VERIFICATION);
    }

    /* Private */

    private ExpressionList<User> getAuthUserFind(
            final AuthUserIdentity identity) {
        return find.where().eq("active", true)
                .eq("linkedAccounts.providerUserId", identity.getId())
                .eq("linkedAccounts.providerKey", identity.getProvider());
    }

    private ExpressionList<User> getEmailUserFind(final String email) {
        return find.where().eq("active", true).ieq("email", email);
    }

    private ExpressionList<User> getUsernamePasswordAuthUserFind(
            final UsernamePasswordAuthUser identity) {
        return getEmailUserFind(identity.getEmail()).ieq(
                "linkedAccounts.providerKey", identity.getProvider());
    }

}
