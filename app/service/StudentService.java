package service;

import com.avaje.ebean.*;
import com.feth.play.module.pa.user.AuthUser;
import models.*;
import play.data.validation.Constraints;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service Layer for Student DB Entity
 */
public class StudentService {

    private final SchoolService schoolService;

    public static class StudentForm {

        public String schoolId;

        @Constraints.Required
        public String name;

        @Constraints.Required
        public boolean releaseForm;

        @Constraints.Required
        public String gender;

        public List<String> nights = new ArrayList<>();

        public List<String> dietary = new ArrayList<>();

        public String dietaryOther;

        public boolean isTeacher = false;

        public StudentForm() {
            gender = "N";
        }

        public StudentForm(Student student) {
            this.schoolId = student.otherId;
            this.name = student.name;
            this.releaseForm = student.releaseForm;
            this.gender = student.gender;

            if (student.nights != null)
                nights = Arrays.asList(student.nights.split(","));
            if (student.dietary != null)
                dietary = Arrays.asList(student.dietary.split(","));
            this.dietaryOther = student.comments;
        }
    }

    public static class TeacherForm extends StudentForm{
        public TeacherForm(){
            super();
            this.isTeacher = true;
            this.releaseForm = true;
        }
        public TeacherForm(Student student){
            super(student);
            this.isTeacher = true;
            this.releaseForm = true;
        }
    }

    @Inject
    public StudentService(final SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    private static final AppModel.Finder<Long, Student> find = new Model.Finder<>(
            Long.class, Student.class);

    /**
     * Creates a new student associated with the given school
     * @param user teacher creating the student
     * @param school school
     * @param schoolId
     *@param s
     * @param releaseForm
     * @param nights
     * @param gender
     * @param dietary
     * @param name name of the student  @return student id
     */
    public Long createForSchool(final User user, final School school, final StudentForm studentForm) {

        final boolean exists = existsForSchool(studentForm.name, school);
        if (exists) return null;
        int ct = find.where()
            .eq("school.id", school.id)
            .ne("deactivated", true)
            .findRowCount();

        if (ct >= 200) return null;

        List<NightCount> counts = getNightCounts();
        if (counts.get(0).c + (studentForm.nights.contains("1") ? 1 : 0) > 180) {
            return null;
        }
        if (counts.get(1).c + (studentForm.nights.contains("2") ? 1 : 0) > 180) {
            return null;
        }

        final Student student = new Student(user, school, studentForm);

        student.createdBy = user;
        student.createdOn = new Date();
        student.isTeacher = studentForm.isTeacher;
        student.save();
        return student.id;

    }

    public Student findForAuthUserIdentity(Long id, AuthUser authUser) {
        final School school = schoolService.findByAuthUserIdentity(authUser);
        return find.where()
                .eq("id", id)
                .eq("school.id", school.id)
                .ne("deactivated", true)
                    .filterMany("sessions")
                    .ne("deactivated", true)
                .findUnique();

    }

    public void delete(Student student) {
        Ebean.delete(student);
    }

    public List<models.Student> getAll() {

        return find.where()
                .ne("deactivated", true)
                    .filterMany("sessions")
                    .ne("deactivated", true)
            .findList();
    }

    public List<models.Student> getAllWithActiveTeacher() {

        return find.where()
                //.ne("deactivated", true)
                //.ge("school.teacher.lastLogin", "2018-01-01 00:00:00")
                //.filterMany("sessions")
                //    .ne("deactivated", true)
                .findList();

    }

    public Long updateStudent(AuthUser authUser, long id, StudentForm formData) {

        List<NightCount> counts = getNightCounts();
        if (counts.get(0).c + (formData.nights.contains("1") ? 1 : 0) > 180) {
            return null;
        }
        if (counts.get(1).c + (formData.nights.contains("2") ? 1 : 0) > 180) {
            return null;
        }

        Student student = findForAuthUserIdentity(id, authUser);
        if (student == null)
            return null;

        boolean wasTeacher = student.isTeacher;
        student.updateWith(formData);
        student.isTeacher = wasTeacher;

        student.save();

        return student.id;

    }

    private boolean existsForSchool(final String name, final School school) {
        return find.where()
                .eq("name", name)
                .isNotNull("school_id")
                .eq("school_id", school.id)
                .ne("deactivated", true)
                .findRowCount() > 0;
    }

    public List<NightCount> getNightCounts() {

        String sqlQuery = "select '1' night, count(*) + (\n" +
                "  SELECT count(1)\n" +
                "  FROM (\n" +
                "    SELECT count(u.email)\n" +
                "    FROM users u\n" +
                "      INNER JOIN schools s\n" +
                "        ON u.id = s.teacher_id\n" +
                "      INNER JOIN students c\n" +
                "        ON s.id = c.school_id\n" +
                "    WHERE\n" +
                "      u.active <> 0 AND s.deactivated <> 1 AND c.deactivated <> 1\n" +
                "      AND locate('1', c.nights) > 0\n" +
                "    GROUP BY u.email\n" +
                "  ) as x\n" +
                ") as c from students ss\n" +
                "where\n" +
                "  ss.deactivated <> 1 and\n" +
                "  locate('1', ss.nights) > 0\n" +
                "union all\n" +
                "select '2' night, count(*) + (\n" +
                "  SELECT count(1)\n" +
                "  FROM (\n" +
                "    SELECT count(u.email)\n" +
                "    FROM users u\n" +
                "      INNER JOIN schools s\n" +
                "        ON u.id = s.teacher_id\n" +
                "      INNER JOIN students c\n" +
                "        ON s.id = c.school_id\n" +
                "    WHERE\n" +
                "      u.active <> 0 AND s.deactivated <> 1 AND c.deactivated <> 1\n" +
                "      AND locate('2', c.nights) > 0\n" +
                "    GROUP BY u.email\n" +
                "  ) as x\n" +
                ") as c from students ss\n" +
                "where\n" +
                "  ss.deactivated <> 1 and\n" +
                "  locate('2', ss.nights) > 0\n";

        List<SqlRow> rows = Ebean.createSqlQuery(
                sqlQuery
        ).findList();

        List<NightCount> nights = new ArrayList<>(2);
        SqlRow row1 = rows.get(0);
        NightCount night1 = new NightCount();
        night1.night = 1;
        try{
            night1.c = Integer.parseInt(row1.get(1).toString());
        }catch (Exception ex){
            night1.c = 0;
        }
        nights.add(night1);
        //nights.set(0, night1);
        SqlRow row2 = rows.get(1);
        NightCount night2 = new NightCount();
        night2.night = 2;
        try{
            night2.c = Integer.parseInt(row2.get(1).toString());
        }catch (Exception ex){
            night2.c = 0;
        }
        nights.add(night2);
       // nights.set(1, night2);
        return nights;
    }

}
