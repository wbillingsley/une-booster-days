package service;

import com.avaje.ebean.Model;
import models.AppModel;
import models.SessionTime;
import models.User;

import java.util.Date;

/**
 * Service Layer for SessionTime DB Entity
 */
public class SessionTimeService {

    private static final AppModel.Finder<Long, SessionTime> find = new Model.Finder<>(
            Long.class, SessionTime.class);

    public SessionTime findForCode(String code) {
        return find.where()
                .eq("code", code)
                .ne("deactivated", true)
                .findUnique();
    }
}
