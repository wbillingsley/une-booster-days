package service;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.service.AbstractUserService;
import com.feth.play.module.pa.user.AuthUser;
import com.feth.play.module.pa.user.AuthUserIdentity;
import models.User;

import javax.inject.Inject;
import javax.inject.Singleton;


/**
 * Service Layer for AuthUser Entity
 */
@Singleton
public class AuthUserService extends AbstractUserService {

	private final UserService userService;

	@Inject
	public AuthUserService(final PlayAuthenticate auth, final UserService userService) {
		super(auth);
		this.userService = userService;
	}

	@Override
	public Object save(final AuthUser authUser) {
		final boolean isLinked = userService.existsByAuthUserIdentity(authUser);
		if (!isLinked) {
			return userService.create(authUser).id;
		} else {
			// we have this user already, so return null
			return null;
		}
	}

	@Override
	public Object getLocalIdentity(final AuthUserIdentity identity) {
		// For production: Caching might be a good idea here...
		// ...and dont forget to sync the cache when users get deactivated/deleted
		final User u = userService.findByAuthUserIdentity(identity);
		if(u != null) {
			return u.id;
		} else {
			return null;
		}
	}

	@Override
	public AuthUser merge(final AuthUser newUser, final AuthUser oldUser) {
		if (!oldUser.equals(newUser)) {
			userService.merge(oldUser, newUser);
		}
		return oldUser;
	}

	@Override
	public AuthUser link(final AuthUser oldUser, final AuthUser newUser) {
		userService.addLinkedAccount(oldUser, newUser);
		return newUser;
	}
	
	@Override
	public AuthUser update(final AuthUser knownUser) {
		// User logged in again, bump last login date
		userService.setLastLoginDate(knownUser);
		return knownUser;
	}

}
