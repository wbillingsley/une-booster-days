package service;

import com.avaje.ebean.Model;
import com.feth.play.module.pa.user.AuthUser;
import models.*;
import play.data.validation.Constraints;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Service layer for Session DB Entity
 */
public class SessionService {

    private final UserService userService;
    private final SessionTimeService sessionTimeService;

    /**
     * Class for session form data
     */
    public static class SessionForm {

        @Constraints.Required
        public String name;

        public String description;

        @Constraints.Required
        public String location;

        @Constraints.Required
        public String timeslot;

        @Constraints.Required
        @Constraints.Min(value = 1, message = "Capacity must be between 1 and 500")
        @Constraints.Max(value = 500, message = "Capacity must be between 1 and 500")
        public int capacity;

        public SessionForm() {

        }

        public SessionForm(Session session) {
            this.name = session.name;
            this.description = session.description;
            this.location = session.location;
            this.timeslot = session.timeslot.code;
            this.capacity = session.capacity;
        }
    }

    /**
     * Session Finder
     */
    private static AppModel.Finder<Long, Session> find = new Model.Finder<>(Long.class, Session.class);

    @Inject
    public SessionService(final UserService userService, final SessionTimeService sessionTimeService) {
        this.userService = userService;
        this.sessionTimeService = sessionTimeService;
    }

    /**
     * Create a new information session
     * @param authUser
     * @param formData
     * @return
     */
    public Long create(AuthUser authUser, SessionService.SessionForm formData) {

        User user = userService.findByAuthUserIdentity(authUser);
        SessionTime sessionTime = sessionTimeService.findForCode(formData.timeslot);

        Session session = new Session(
                formData.name,
                formData.description,
                formData.location,
                sessionTime,
                formData.capacity,
                user
        );

        session.save();

        return session.id;

    }

    public void deleteById(Long id) {
        Session session = findById(id);
        if (session != null)
            session.delete();
    }

    /**
     * Finds a session record by id
     * @param id
     * @return
     */
    public Session findById(Long id) {
        return find.where()
                .eq("id", id)
                .ne("deactivated", true)
                    .filterMany("students")
                    .ne("deactivated", true)
                .findUnique();
    }


    /**
     * Get all sessions in database (note, not filtered - best not used publicly)
     * @return
     */
    public List<Session> getAll() {
        List<Session> s = find.where()
            .ne("deactivated", true)
                .filterMany("students")
                .ne("deactivated", true)
            .findList();
	s.sort((a, b) -> a.timeslot.startTime.compareTo(b.timeslot.startTime));
	return s;
    }

    /**
     * Update session entry with form data
     * @param formData
     * @return
     */
    public Long updateSession(User user, Long id, SessionForm formData) {

        Session session = findById(id);

        if (session.students.size() > formData.capacity) {
            // TODO: Validation if students are over registered
        }

        for (Student student : session.students) {
            // TODO: Check each student for clashes against other sessions
        }

        SessionTime time = SessionTime.find.where()
                .ne("deactivated", true)
                .eq("code", formData.timeslot)
                .findUnique();

        session.capacity = formData.capacity;
        session.description = formData.description;
        session.location = formData.location;
        session.name = formData.name;
        session.timeslot = time;

        session.modifiedBy = user;
        session.modifiedOn = new Date();

        session.save();

        return session.id;

    }

}
