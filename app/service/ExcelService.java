package service;

import models.Student;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Service Layer for Excel IO
 */
public class ExcelService {

    /**
     * Read a list of student names from an excel file into a list
     * @param file
     * @return
     * @throws Exception
     */
    public static List<StudentService.StudentForm> readStudentNames(File file) throws Exception {

        List<StudentService.StudentForm> studentForms = new ArrayList<>();

        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);

        /* Find header */
        Iterator<Row> iterator = sheet.iterator();
        Boolean foundHeader = false;
        while (iterator.hasNext()) {
            Row headerRow = iterator.next();
            short firstCellIndex = headerRow.getFirstCellNum();
            Cell headerCell = headerRow.getCell(firstCellIndex);
            String header = headerCell.getStringCellValue();
            if (header.equalsIgnoreCase("School Id (Optional)")) {
                System.out.println("Template header found.");
                foundHeader = true;
                iterator.next(); // Skip merged cell.
                break;
            }
        }
        if (!foundHeader)
            throw new Exception("Invalid template.");

        /* Iterate Rows */
        while (iterator.hasNext()) {
            Row studentRow = iterator.next();

	          int i = 0;


            Cell idCell = studentRow.getCell(i++);
            Cell nameCell = studentRow.getCell(i++);
            Cell releaseFormCell = studentRow.getCell(i++);
            Cell genderCell = studentRow.getCell(i++);
            Cell night1Cell = studentRow.getCell(i++);
            Cell night2Cell = studentRow.getCell(i++);
            Cell glutenFreeCell = studentRow.getCell(i++);
            Cell halalCell = studentRow.getCell(i++);
            Cell lactoseFreeCell = studentRow.getCell(i++);
            Cell vegetarianCell = studentRow.getCell(i++);
            Cell dietaryOtherCell = studentRow.getCell(i++);

            String id = null;
            try {
                id = String.valueOf(idCell.getStringCellValue());
            } catch (Exception ex) { }
            if (id == null) {
                try {
                    id = String.valueOf(idCell.getNumericCellValue());
                } catch (Exception ex) { }
            }

            String name;
            try {
                name = nameCell.getStringCellValue();
            } catch (Exception ex) {
                throw new Exception("Invalid name on row '" + studentRow.getRowNum() + "'");
            }
            Boolean releaseForm = isTruthy(getCellValue(releaseFormCell), "Release Form", studentRow.getRowNum());

            String gender = getCellValue(genderCell, Student.NO_GENDER);
            if (gender == null || gender.trim().isEmpty())
                gender = models.Student.NO_GENDER;
            gender = gender.toUpperCase();

            List<String> nights = new ArrayList<>();
            if (isTruthy(getCellValue(night1Cell), "Night 1", studentRow.getRowNum())) nights.add(Student.FIRST_NIGHT);
            if (isTruthy(getCellValue(night2Cell), "Night 2", studentRow.getRowNum())) nights.add(Student.SECOND_NIGHT);

            List<String> dietary = new ArrayList<>();
            if (isTruthy(getCellValue(glutenFreeCell), "Gluten Free", studentRow.getRowNum()))
                dietary.add(Student.GLUTEN_FREE);
            if (isTruthy(getCellValue(halalCell), "Halal", studentRow.getRowNum()))
                dietary.add(Student.HALAL);
            if (isTruthy(getCellValue(lactoseFreeCell), "Lactose Free", studentRow.getRowNum()))
                dietary.add(Student.LACTOSE_FREE);
            if (isTruthy(getCellValue(vegetarianCell), "Vegetarian", studentRow.getRowNum()))
                dietary.add(Student.VEGETARIAN);

            StudentService.StudentForm studentForm = new StudentService.StudentForm();
            studentForm.schoolId = id;
            studentForm.name = name;
            studentForm.releaseForm = releaseForm;
            studentForm.gender = gender;
            studentForm.nights = nights;
            studentForm.dietary = dietary;
            System.out.println(String.join(",", studentForm.dietary));

            String dietaryOther = getCellValue(dietaryOtherCell);
            studentForm.dietaryOther = dietaryOther;

            studentForms.add(studentForm);

        }

        workbook.close();
        inputStream.close();

        return studentForms;

    }

    /**
     * Read the session table from an excel file for uploading
     * @param file
     * @return
     * @throws Exception
     */
    public static List<SessionService.SessionForm> readSessionData(File file) throws Exception {
        List<SessionService.SessionForm> sessions = new ArrayList<>();

        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();

        int r = 0;

        while (iterator.hasNext()) {

            r++;
            int c = 0;
            try {
                Row nextRow = iterator.next();
                if (nextRow.getLastCellNum() < 5) continue;

                SessionService.SessionForm session = new SessionService.SessionForm();

                Cell nameCell = nextRow.getCell(c++);
                Cell descCell = nextRow.getCell(c++);
                Cell locaCell = nextRow.getCell(c++);
                Cell timeCell = nextRow.getCell(c++);
                Cell capaCell = nextRow.getCell(c++);

                session.name = getCellValue(nameCell);
                session.description = getCellValue(descCell);
                session.location = getCellValue(locaCell);
                session.timeslot = getCellValue(timeCell);
                session.capacity = (int)capaCell.getNumericCellValue();

                sessions.add(session);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception("Failed to read row '" + r + "'");
            }

        }

        workbook.close();
        inputStream.close();
        inputStream.close();

        return sessions;

    }

    private static String getCellValue(Cell cell) {
        return getCellValue(cell, "");
    }

    private static String getCellValue(Cell cell, String def) {
        if (cell == null || cell.getCellTypeEnum() == CellType.BLANK || cell.getCellTypeEnum() == CellType._NONE)
            return def;
        return cell.getStringCellValue();
    }

    private static Boolean isTruthy(String text, String type, int rowNum) throws Exception {
        String value = text.trim();
        System.out.println(type + " - " + " " + String.valueOf(rowNum) + " " + text);
        if (value.isEmpty()) return false;
        if (value.equalsIgnoreCase("y") || value.equalsIgnoreCase("yes") ||
                value.equalsIgnoreCase("true") || value.equalsIgnoreCase("t") ||
                value.equalsIgnoreCase("1") || value.equalsIgnoreCase("x")) return true;
        if (value.equalsIgnoreCase("n") || value.equalsIgnoreCase("no") ||
                value.equalsIgnoreCase("0")) return false;
        throw new Exception(
                "Unknown value.  Expected 'x' or no value to indicate choice for '"
                        + type + "' row '" + String.valueOf(rowNum + 2) + "'.");
    }
}
