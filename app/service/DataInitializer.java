package service;

import com.avaje.ebean.Model;
import com.feth.play.module.pa.PlayAuthenticate;
import controllers.Application;
import models.*;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Data initializer class.
 */
public class DataInitializer {
    @Inject
    public DataInitializer() {
        if (SecurityRole.find.findRowCount() == 0) {
            for (final String roleName : Arrays
                    .asList(Application.USER_ROLE, Application.ADMIN_ROLE, Application.REPORT_ROLE)) {
                final SecurityRole role = new SecurityRole();
                role.roleName = roleName;
                role.save();
            }
        }
        if (SessionTime.find.findRowCount() == 0) {

            Calendar c = Calendar.getInstance();
            c.set(2017, 6, 5, 9, 30, 0);
            Date start = c.getTime();
            c.add(Calendar.HOUR, 1);
            Date end = c.getTime();
            SessionTime sessionTime = new SessionTime();
            sessionTime.name = "June 5 9:30-10:30am";
            sessionTime.code = "J5_S1";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 5, 11, 00, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 5 11:00am-12:00pm";
            sessionTime.code = "J5_S2";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 5, 13, 15, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 5 1:15-2:15pm";
            sessionTime.code = "J5_S3";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 5, 14, 30, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 5 2:30-3:30pm";
            sessionTime.code = "J5_S4";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c = Calendar.getInstance();
            c.set(2017, 6, 6, 9, 30, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 6 9:00-10:00am";
            sessionTime.code = "J6_S1";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 6, 11, 00, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 6 10:45am-11:45pm";
            sessionTime.code = "J6_S2";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 6, 13, 15, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 6 1:00-2:00pm";
            sessionTime.code = "J6_S3";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

            c.set(2017, 6, 6, 14, 30, 0);
            start = c.getTime();
            c.add(Calendar.HOUR, 1);
            end = c.getTime();
            sessionTime = new SessionTime();
            sessionTime.name = "June 6 2:30-3:30pm";
            sessionTime.code = "J6_S4";
            sessionTime.startTime = start;
            sessionTime.endTime = end;
            sessionTime.save();

        }
    }

    private static SessionTime construct(String code) {
        SessionTime time = new SessionTime();
        time.name = code;
        time.code = code;
        // TODO: start, end times
        time.startTime = new Date();
        time.endTime = new Date();
        return time;
    }
}
