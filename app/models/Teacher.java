package models;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name = "teachers")
public class Teacher extends AppModel {

  @Id
  public Long id;

  public User user;

  @ManyToOne
  public School school;

}
