package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.format.Formats;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "session_times")
public class SessionTime extends AppModel {


  public static AppModel.Finder<Long, SessionTime> find = new Model.Finder<>(Long.class, SessionTime.class);

  @Id
  public Long id;

  public String name;

  public String code;

  @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date startTime;

  @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date endTime;

  @JsonIgnore
  public boolean deactivated;

  @JsonIgnore
  @OneToMany
  public List<Session> sessions;

}
