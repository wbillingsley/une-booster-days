package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.format.Formats;
import service.StudentService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "students")
public class Student extends AppModel {

    /* Gender Constants */
    public static final String NO_GENDER = "N";
    public static final String MALE = "M";
    public static final String FEMALE = "F";

    /* Dietary Constants */
    public static final String GLUTEN_FREE = "G";
    public static final String HALAL = "H";
    public static final String LACTOSE_FREE = "L";
    public static final String VEGETARIAN = "V";

    /* Accommodation Constants */
    public static final String FIRST_NIGHT = "1";
    public static final String SECOND_NIGHT = "2";

    @Id
    public Long id;

    @JsonIgnore
    public Boolean isTeacher;

    @JsonIgnore
    public String otherId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String name;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String gender;

    public String nights;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String dietary;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String comments;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public boolean releaseForm;

    @JsonIgnore
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date createdOn;

    @JsonIgnore
    public User createdBy;

    @JsonIgnore
	public boolean deactivated;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String schoolName() {
        return school.name;
    }

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String teacherName() {
	try{
        return school.teacher.name;}catch(Exception e){return "";}
    }

    @JsonIgnore
    @ManyToOne
    public School school;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    public List<Session> sessions = new ArrayList<>();

    @JsonProperty
    public List<String> sessionTimes() {
        if (sessions == null) return new ArrayList<>();
        List<String> codes = sessions.stream()
                .map(session -> session.timeslot.code)
                .collect(Collectors.toList());
        return codes;
    }

    @JsonProperty
    public List<Long> sessionIds() {
        if (sessions == null) return new ArrayList<>();
        List<Long> ids = sessions.stream()
                .map(session -> session.id)
                .collect(Collectors.toList());
        return ids;
    }

    public Student() {

    }

    public Student(User user, School school, StudentService.StudentForm studentForm) {
        this.school = school;
        this.createdBy = user;
        this.createdOn = new Date();
        updateWith(studentForm);
    }

    private static String toCSV(List<String> strings) {
        if (strings == null || strings.size() == 0) return "";
        return String.join(",", strings);
    }

    public void updateWith(StudentService.StudentForm studentForm) {

        this.otherId = studentForm.schoolId;
        this.isTeacher = studentForm.isTeacher;
        this.name = studentForm.name;
        this.releaseForm = studentForm.releaseForm;
        this.gender = studentForm.gender;
        this.nights = toCSV(studentForm.nights);
        this.dietary = toCSV(studentForm.dietary);
        this.comments = studentForm.dietaryOther;

    }
}
