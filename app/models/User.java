package models;

import be.objectify.deadbolt.java.models.Permission;
import be.objectify.deadbolt.java.models.Role;
import be.objectify.deadbolt.java.models.Subject;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.*;

/**
 * Initial version based on work by Steve Chaloner (steve@objectify.be) for
 * Deadbolt2
 */
@Entity
@Table(name = "users")
public class User extends AppModel implements Subject {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Constraints.Email
	// if you make this unique, keep in mind that users *must* merge/link their
	// accounts then on signup with additional providers
	// @Column(unique = true)
	public String email;

	public String name;

	@JsonIgnore
	public String firstName;

	@JsonIgnore
	public String lastName;

	@JsonIgnore
	@Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date lastLogin;

	@JsonIgnore
	public boolean active;

	@JsonIgnore
	public boolean emailValidated;

	@JsonIgnore
	@ManyToMany
	public List<SecurityRole> roles;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	public List<LinkedAccount> linkedAccounts;

	@JsonIgnore
	@ManyToMany
	public List<UserPermission> permissions;

	@JsonIgnore
	@Override
	public String getIdentifier()
	{
		return Long.toString(this.id);
	}

	@JsonIgnore
	@Override
	public List<? extends Role> getRoles() {
		return this.roles;
	}

	@JsonIgnore
	@Override
	public List<? extends Permission> getPermissions() {
		return this.permissions;
	}

	public void changePassword(final UsernamePasswordAuthUser authUser,
							   final boolean create) {

		LinkedAccount a = getAccountByProvider(authUser.getProvider());
		if (a == null) {
			if (create) {
				a = LinkedAccount.create(authUser);
				a.user = this;
			} else {
				throw new RuntimeException(
						"Account not enabled for password usage");
			}
		}
		a.providerUserId = authUser.getHashedPassword();
		a.save();
	}

	public LinkedAccount getAccountByProvider(final String providerKey) {
		return LinkedAccount.findByProviderKey(this, providerKey);
	}

	@JsonIgnore
	public Set<String> getProviders() {
		final Set<String> providerKeys = new HashSet<String>(
				this.linkedAccounts.size());
		for (final LinkedAccount acc : this.linkedAccounts) {
			providerKeys.add(acc.providerKey);
		}
		return providerKeys;
	}

	public void merge(final User otherUser) {
		for (final LinkedAccount acc : otherUser.linkedAccounts) {
			this.linkedAccounts.add(LinkedAccount.create(acc));
		}
		// do all other merging stuff here - like resources, etc.

		// deactivate the merged user that got added to this one
		otherUser.active = false;
		Arrays.asList(new User[] { otherUser, this }).forEach(u -> Ebean.save(u));
	}

	public void resetPassword(final UsernamePasswordAuthUser authUser,
							  final boolean create) {
		// You might want to wrap this into a transaction
		this.changePassword(authUser, create);
		TokenAction.deleteByUser(this, TokenAction.Type.PASSWORD_RESET);
	}

}
