package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.feth.play.module.pa.user.AuthUser;
import play.data.format.Formats;
import service.SessionService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "sessions")
public class Session extends AppModel {

    @Id
    public Long id;

    public String name;

    public String description;

    public String location;

    public int capacity;

    @ManyToOne
    public SessionTime timeslot;

    @ManyToMany(mappedBy = "sessions")
    public List<Student> students = new ArrayList<>();

    @JsonProperty
    public List<Long> studentIds() {
        if (students == null) return new ArrayList<>();
        List<Long> ids = students.stream()
                .map(student -> student.id)
                .collect(Collectors.toList());
        return ids;
    }

    @JsonIgnore
    public User createdBy;

    @JsonIgnore
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date createdOn;

    @JsonIgnore
    public User modifiedBy;

    @JsonIgnore
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date modifiedOn;

    @JsonIgnore
	public boolean deactivated;

    @JsonProperty
    public int studentCount() {
        return students.size();
    }

    public Session(String name, String description,
                   String location, SessionTime timeslot,
                   int capacity, User user) {

        this.name = name;
        this.description = description;
        this.location = location;
        this.timeslot = timeslot;
        this.capacity = capacity;
        this.createdBy = user;
        this.createdOn = new Date();
    }

}
