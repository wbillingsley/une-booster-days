package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.feth.play.module.pa.user.AuthUser;
import play.data.format.Formats;
import service.SchoolService;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Table(name = "schools")
public class School extends AppModel {

  private static final long serialVersionUID = 1L;

  public static final String SCHOOL_DETAILS_EMPTY = "School details not stored. Please register your school details to continue.";

  @Id
  public Long id;

  public String name;

  public String streetAddress;

  public String townSuburb;

  public String state;

  public String postCode;

  @JsonIgnore
  public User createdBy;

  @JsonIgnore
  @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date createdOn;

  @JsonIgnore
  public User modifiedBy;

  @JsonIgnore
  @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date modifiedOn;

  @OneToOne
  public User teacher;

  // @OneToMany(cascade=CascadeType.PERSIST)
  // public List<User> colleagues;

  @OneToMany
  public List<Student> students;

  @JsonProperty
  public int studentCount() {
      if (students == null) return 0;
      return students.size();
  }

//  @JsonProperty
//  public Map<String, Integer> studentSummary() {
//      Map<String, Integer> map = new HashMap<>();
//      map.put("M", 0);
//      map.put("F", 0);
//      map.put("N", 0);
//      map.put("1", 0);
//      map.put("2", 0);
//      map.put("T", 0);
//      if (this.students == null) return map;
//      this.students.forEach(x->{
//          map.put(x.gender, map.get(x.gender));
//          if (x.nights.contains("1")) {
//              map.put("1", map.get("1"));
//          }
//          if (x.nights.contains("2")) {
//              map.put("2", map.get("2"));
//          }
//      });
//      map.put("T", studentCount());
//      return map;
//  }



  public School() {

  }

  public School(final String name, final String streetAddress,
                final String townSuburb, final String state,
                final String postCode, final User createdBy) {

    this.name = name;
    this.streetAddress = streetAddress;
    this.townSuburb = townSuburb;
    this.state = state;
    this.postCode = postCode;
    this.teacher = createdBy;
    this.createdBy = createdBy;
    this.createdOn = new Date();

  }

}
