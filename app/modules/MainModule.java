package modules;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import com.feth.play.module.mail.IMailer;
import com.feth.play.module.mail.Mailer;
import com.feth.play.module.mail.Mailer.MailerFactory;
import com.feth.play.module.pa.Resolver;
import com.feth.play.module.pa.service.UserService;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.slf4j.LoggerFactory;
import providers.MyUsernamePasswordAuthProvider;
import service.AuthUserService;
import service.DataInitializer;
import service.MyResolver;

/**
 * Initial DI module.
 */
public class MainModule extends AbstractModule {

	@Override
	protected void configure() {

		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		// print logback's internal status
		StatusPrinter.print(lc);

		install(new FactoryModuleBuilder().implement(IMailer.class, Mailer.class).build(MailerFactory.class));

		bind(Resolver.class).to(MyResolver.class);

		bind(DataInitializer.class).asEagerSingleton();

		bind(UserService.class).to(AuthUserService.class).asEagerSingleton();

		bind(MyUsernamePasswordAuthProvider.class).asEagerSingleton();

	}

}
