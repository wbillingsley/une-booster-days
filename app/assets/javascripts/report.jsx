class Sessions extends React.Component {
    render() {
        return (
            <div>
                { this.props.sessions
                    .filter(x => x.timeslot !== null)
                    .sort((a,b)=>a.timeslot.code + a.name < b.timeslot.code + b.name ? -1 : 1)
                    .map(x=><Session data={x} />) }
            </div>
        );
    }
}
class Dietary extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            Dietary Requirements Listing
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="container">
                                    TODO
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
class Session extends React.Component {
    render() {
        var data = this.props.data;
        var content = this.props.data.students.length == 0 ? (
            <h5>No students registered for this session</h5>
        ) : (

            <table className="table table-condensed">
                <tbody>
                { data.students
                    .sort((a, b)=>a.schoolName < b.schoolName ? -1 : 1)
                    .map(x=><tr>
                        <td>{x.name}</td>
                        <td>{x.schoolName} ({x.teacherName})</td>
                    </tr>) }
                </tbody>
            </table>
        );
        return (
            <div className="container">
                <div className="row">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            Session Listing: { data.name } ({ data.timeslot.name })
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="container">
                                    { content }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
class Student extends React.Component {
    render() {
      var nights = this.props.data.nights||'';
      var has_N1 = nights.indexOf('1')>-1;
      var has_N2 = nights.indexOf('2')>-1;
      var has_J5_S1 = this.props.data.sessionTimes.find(x=>x=="19_J3_S1");
      var has_J5_S2 = this.props.data.sessionTimes.find(x=>x=="19_J3_S2");
      var has_J5_S3 = this.props.data.sessionTimes.find(x=>x=="19_J3_S3");
      var has_J5_S4 = this.props.data.sessionTimes.find(x=>x=="19_J3_S4");
      var has_J5_S5 = this.props.data.sessionTimes.find(x=>x=="19_J3_S5");
      var has_J5_S6 = this.props.data.sessionTimes.find(x=>x=="19_J3_S6");
      var has_J6_S1 = this.props.data.sessionTimes.find(x=>x=="19_J4_S1");
      var has_J6_S2 = this.props.data.sessionTimes.find(x=>x=="19_J4_S2");
      var has_J6_S3 = this.props.data.sessionTimes.find(x=>x=="19_J4_S3");
      var has_J6_S4 = this.props.data.sessionTimes.find(x=>x=="19_J4_S4");
      var has_J6_S5 = this.props.data.sessionTimes.find(x=>x=="19_J4_S5");
      var has_special = this.props.data.sessionTimes.find(x=>x=="19_J4_SP");
        return (
            <tr>
                <td>{this.props.data.schoolName}</td>
                <td>{this.props.data.teacherName}</td>
                <td>{this.props.data.name}{this.props.data.isTeacher?'<br />Staff Member':''}</td>
                <td style={{background: this.props.data.gender=='N'?'yellow':''}}>{this.props.data.gender}</td>
                <td style={{background: has_N1?'lightgreen':''}}>{has_N1?'Y':''}</td>
                <td style={{background: has_N2?'lightgreen':''}}>{has_N2?'Y':''}</td>
                <td style={{background: (this.props.data.dietary||'').length?'orange':''}}>{this.props.data.dietary}</td>
                <td style={{background: (this.props.data.comments||'').length?'orange':''}}>{this.props.data.comments}</td>
                <td style={{background: this.props.data.releaseForm?'':'red'}}>{this.props.data.releaseForm}</td>
                <td style={{background: has_J5_S1?'lightgreen':''}}>{has_J5_S1?'Y':''}</td>
                <td style={{background: has_J5_S2?'lightgreen':''}}>{has_J5_S2?'Y':''}</td>
                <td style={{background: has_J5_S3?'lightgreen':''}}>{has_J5_S3?'Y':''}</td>
                <td style={{background: has_J5_S4?'lightgreen':''}}>{has_J5_S4?'Y':''}</td>
                <td style={{background: has_J5_S5?'lightgreen':''}}>{has_J5_S5?'Y':''}</td>
                <td style={{background: has_J5_S6?'lightgreen':''}}>{has_J5_S6?'Y':''}</td>
                <td style={{background: has_J6_S1?'lightgreen':''}}>{has_J6_S1?'Y':''}</td>
                <td style={{background: has_J6_S2?'lightgreen':''}}>{has_J6_S2?'Y':''}</td>
                <td style={{background: has_J6_S3?'lightgreen':''}}>{has_J6_S3?'Y':''}</td>
                <td style={{background: has_J6_S4?'lightgreen':''}}>{has_J6_S4?'Y':''}</td>
                <td style={{background: has_J6_S5?'lightgreen':''}}>{has_J6_S5?'Y':''}</td>
                <td style={{background: has_special?'lightgreen':''}}>{has_special?'Y':''}</td>
            </tr>
        );
    }
}
class Students extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            School Listing
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="container">
                                    <table className="table table-bordered table-condensed" style={{fontSize: '10px'}}>
                                        <thead>
                                        <tr>
                                            <th>School</th>
                                            <th>Co-ordinator</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Night 1</th>
                                            <th>Night 2</th>
                                            <th>Dietary</th>
                                            <th>Comments</th>
                                            <th>Release</th>
                                            <th>J3_S1</th>
                                            <th>J3_S2</th>
                                            <th>J3_S3</th>
                                            <th>J3_S4</th>
                                            <th>J3_S5</th>
                                            <th>J3_S6</th>
                                            <th>J4_S1</th>
                                            <th>J4_S2</th>
                                            <th>J4_S3</th>
                                            <th>J4_S4</th>
                                            <th>J4_S5</th>
                                            <th>SPECIAL</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.students
                                                .map(data => <Student data={data} />)
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
class Schools extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            School Listing
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="container">
                                    <table className="table table-bordered" style={{fontSize: '10px'}}>
                                        <thead>
                                        <tr>
                                            <th rowSpan="2" style={{verticalAlign: 'middle'}}>School</th>
                                            <th colSpan="2" rowSpan="2" style={{verticalAlign: 'middle'}}>Teacher</th>
                                            <th rowSpan="2" style={{verticalAlign: 'middle'}}>Address</th>
                                            <th colSpan="4" style={{verticalAlign: 'middle'}}>Student/Staff Count</th>
                                            <th colSpan="6" style={{verticalAlign: 'middle'}}>Accommodation</th>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <th>M</th>
                                            <th>F</th>
                                            <th>N</th>
                                            <th colSpan="3">June 3 - M/F/N</th>
                                            <th colSpan="3">June 3 - M/F/N</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.schools
                                                .map(data => <School data={data} />)
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
class School extends React.Component {
    render() {
        var data = this.props.data;
        return <tr>
            <td>{data.name}</td>
            <td>{data.teacher.name}</td>
            <td><a href={"mailto:" + data.teacher.email}><i className="glyphicon glyphicon-envelope"></i></a></td>
            <td>{data.streetAddress}, {data.townSuburb} {data.state}</td>
            <td>{data.students.length}</td>
            <td>{data.students.filter(x=>x.gender == 'M' && (!x.isTeacher)).length}/{data.students.filter(x=>x.gender == 'M' && x.isTeacher).length}</td>
            <td>{data.students.filter(x=>x.gender == 'F' && (!x.isTeacher)).length}/{data.students.filter(x=>x.gender == 'F' && x.isTeacher).length}</td>
            <td>{data.students.filter(x=>x.gender == 'N' && (!x.isTeacher)).length}/{data.students.filter(x=>x.gender == 'N' && x.isTeacher).length}</td>
            <td>{data.students.filter(x=>x.gender == 'M' && (x.nights||"").indexOf("1")>-1).length}</td>
            <td>{data.students.filter(x=>x.gender == 'F' && (x.nights||"").indexOf("1")>-1).length}</td>
            <td>{data.students.filter(x=>x.gender == 'N' && (x.nights||"").indexOf("1")>-1).length}</td>
            <td>{data.students.filter(x=>x.gender == 'M' && (x.nights||"").indexOf("2")>-1).length}</td>
            <td>{data.students.filter(x=>x.gender == 'F' && (x.nights||"").indexOf("2")>-1).length}</td>
            <td>{data.students.filter(x=>x.gender == 'N' && (x.nights||"").indexOf("2")>-1).length}</td>
        </tr>;
    }
}
class Panel extends React.Component {
    render() {
        return (
            <div className="row" style={{background: 'white', marginBottom: '20px'}}>
                <div className="container">
                    <div className="row" style={{marginTop: '20px', marginBottom: '15px'}}>
                        {
                            this.props.elements.map((x, i, a) =>
                                <Pane data={x} index={i} set={a}/>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
Panel.defaultProps = {
    elements: []
}
class Pane extends React.Component {
    render() {
        var data = this.props.data;
        return (
            <div className="col-xxs-6 col-xs-2 dash-pane">
                <div className="dash-pane-label">
                    { data.label }
                </div>
                <div className="dash-pane-value">
                    { data.value }
                </div>
                <div className="dash-pane-sublabel">
                    { data.sublabel }
                </div>
            </div>
        );
    }
}
Pane.defaultProps = {
    set: []
}
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            schools: [],
            students: []
        };
    }
    componentDidMount() {
        $.ajax("./api/graph")
            .then(([schools,sessions,students]) => {
                this.setState({schools, sessions, students});
            }, err => {
                this.setState({
                    error: {
                        source: 'displaying session and student data',
                        err: err
                    }
                });
            })
            .fail(err=> {
                this.setState({
                    error: {
                        source: 'downloading session and student data',
                        err: err
                    }
                });
            })
            .always(()=> {
                this.setState({loading: false});
            });
    }
    render() {
        if (this.state.error) {
            return (
                <div>
                    <h2>Error {this.state.error.source}</h2>
                </div>
            )
        };
        if (this.state.loading) {
            return (
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6 col-sm-offset-3 col-xss col-xs-12">
                                <h2>Loading</h2>
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
        var elements = [{
            label: 'Teachers',
            sublabel: 'Registered',
            value: this.state.schools.filter(x=>x.studentCount).length
        },{
            label: 'Students',
            sublabel: 'Registered',
            value: this.state.schools
                .map(x=>x.students.length)
                .reduce((a,b)=>a + b)
        },{
            sublabel: 'Accomodation',
            label: 'June 3',
            value: this.state.schools
                .map(x=>x.students.filter(y=>y.nights.indexOf("1") != -1).length)
                .reduce((a,b)=>a+b)
        },{
            sublabel: 'Attendance',
            label: 'June 3',
            value: this.state.schools
                .map(x=>x.students.filter(y=>y.sessionTimes.find(z=>z.startsWith('19_J3'))).length)
                .reduce((a,b)=>a+b)
        },{
            sublabel: 'Accomodation',
            label: 'June 3',
            value: this.state.schools
                .map(x=>x.students.filter(y=>y.nights.indexOf("2") != -1).length)
                .reduce((a,b)=>a+b)
        },{
            sublabel: 'Attendance',
            label: 'June 4',
            value: this.state.schools
                .map(x=>x.students.filter(y=>y.sessionTimes.find(z=>z.startsWith('19_J4'))).length)
                .reduce((a,b)=>a+b)
        }];
        var emails = this.state.schools.reduce((a,b)=>`${a}${b.teacher.email},`, '');
	var emailLink = `mailto:?bcc=${emails}`;
        return (
        <div>
            <Panel elements={elements} />
            <Schools schools={this.state.schools} />
            <Students students={this.state.students} />
            <Dietary />
            <Sessions sessions={this.state.sessions} />
        </div>
        );
    }
}

var mountNode = document.getElementById("react-mount");
ReactDOM.render(<App />, mountNode);
