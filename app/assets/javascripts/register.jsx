"use strict";var _createClass=function(){function c(d,f){for(var h,g=0;g<f.length;g++)h=f[g],h.enumerable=h.enumerable||!1,h.configurable=!0,"value"in h&&(h.writable=!0),Object.defineProperty(d,h.key,h)}return function(d,f,g){return f&&c(d.prototype,f),g&&c(d,g),d}}();function _classCallCheck(c,d){if(!(c instanceof d))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(c,d){if(!c)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return d&&("object"==typeof d||"function"==typeof d)?d:c}function _inherits(c,d){if("function"!=typeof d&&null!==d)throw new TypeError("Super expression must either be null or a function, not "+typeof d);c.prototype=Object.create(d&&d.prototype,{constructor:{value:c,enumerable:!1,writable:!0,configurable:!0}}),d&&(Object.setPrototypeOf?Object.setPrototypeOf(c,d):c.__proto__=d)}var Student=function(c){function d(f){_classCallCheck(this,d);var g=_possibleConstructorReturn(this,(d.__proto__||Object.getPrototypeOf(d)).call(this,f));return g.state={expanded:!1,sessionFilter:null},g.remove=g.remove.bind(g),g.onRemove=g.onRemove.bind(g),g.onExpand=g.onExpand.bind(g),g}return _inherits(d,c),_createClass(d,[{key:"onRemove",value:function onRemove(f){this.props.onRemove&&this.props.onRemove(f)}},{key:"onExpand",value:function onExpand(){this.setState({expanded:!this.state.expanded})}},{key:"remove",value:function remove(){var j=this;if(!this.state.thinking){var f=this.props.sessionId,g=this.props.data.id;this.setState({thinking:!0},function(){return $.ajax("./api/sessions/"+f+"/students/"+g+"/remove",{method:"POST"}).then(function(){j.onRemove({sessionId:f,studentId:g})},function(){alert("error")}).fail(function(){alert("error")}).always(function(){j.setState({thinking:!1})})})}}},{key:"render",value:function render(){var f=this.props.data,g=this.props.activeSessions.find(function(k){return f.sessionIds.find(function(l){return l==k.id})});if(!this.props.removable&&g)return null;var h=this.props.removable?React.createElement("button",{className:"btn btn-default btn-xs pull-right",style:{border:"none",marginRight:"-5px"},onClick:this.remove},React.createElement("i",{className:"glyphicon glyphicon-remove",style:{fontSize:"9px",paddingRight:"1px"}})):null,j=this.props.removable?null:g?null:React.createElement("i",{className:"glyphicon glyphicon-th",style:{marginRight:"7px"}});return React.createElement("li",{className:"list-group-item","data-student-id":f.id},j,f.name,h)}}]),d}(React.Component);Student.defaultProps={activeSessions:[]};var Students=function(c){function d(f){_classCallCheck(this,d);var g=_possibleConstructorReturn(this,(d.__proto__||Object.getPrototypeOf(d)).call(this,f));return g.state={hideFullStudents:!1,sessionFilter:null},g}return _inherits(d,c),_createClass(d,[{key:"render",value:function render(){var f=this.props.sessionFilter,g=this.props.sessions.filter(function(h){return h.timeslot.code==f});return React.createElement("div",{"class":"row",style:{overflowY:"scroll",height:"500px",paddingBottom:"250px"}},React.createElement("ul",{className:"list-group dragula-container student-list"},this.props.data.sort(function(h,j){return h.name<j.name?-1:j.name<h.name?1:0}).sort(function(h){return h.sessionTimes.find(function(k){return k==f})?1:-1}).map(function(h){return React.createElement(Student,{data:h,activeSessions:g,sessionFilter:f})})))}}]),d}(React.Component);Students.defaultProps={data:[],sessions:[]};var Session=function(c){function d(f){_classCallCheck(this,d);var g=_possibleConstructorReturn(this,(d.__proto__||Object.getPrototypeOf(d)).call(this,f));return g.onRemoveStudent=g.onRemoveStudent.bind(g),g}return _inherits(d,c),_createClass(d,[{key:"onRemoveStudent",value:function onRemoveStudent(f){this.props.onRemoveStudent&&this.props.onRemoveStudent(f)}},{key:"render",value:function render(){var k=this,f=this.props.data,g=this.props.students.filter(function(l){return l.sessionIds.find(function(m){return m==f.id})}),h=100*(f.studentCount/f.capacity),j=0.2<h?f.studentCount+"/"+f.capacity:null;return React.createElement("li",{className:"list-group-item dragula-container","data-session-id":f.id,style:{margin:"10px 0"}},React.createElement("a",{name:f.id,style:{position:"absolute",top:"-10px"}}),f.name,g.map(function(l){return React.createElement(Student,{data:l,removable:!0,sessionId:f.id,onRemove:k.onRemoveStudent})}),React.createElement("div",{className:"progress",style:{marginTop:"15px",marginBottom:"0px"}},React.createElement("div",{className:"progress-bar",style:{width:h+"%"}},j)))}}]),d}(React.Component);Session.defaultProps={students:[]};var Sessions=function(c){function d(f){_classCallCheck(this,d);var g=_possibleConstructorReturn(this,(d.__proto__||Object.getPrototypeOf(d)).call(this,f));return g.state={filterText:null},g.filterTime=g.filterTime.bind(g),g.onRemoveStudent=g.onRemoveStudent.bind(g),g.onSessionFilterUpdated=g.onSessionFilterUpdated.bind(g),g}return _inherits(d,c),_createClass(d,[{key:"componentWillReceiveProps",value:function componentWillReceiveProps(f){if(!this.state.codes&&f.data&&f.data.length){var g=[];f.data.map(function(j){return j.timeslot}).filter(function(j){return j}).forEach(function(j){g.find(function(k){return k.code==j.code})||g.push(j)}),g=g.sort(function(j,k){return j.code<k.code?-1:k.code<j.code?1:0});var h=null;g.length&&(h=g[0].code),this.setState({codes:g,filterTime:h},this.onSessionFilterUpdated)}}},{key:"onSessionFilterUpdated",value:function onSessionFilterUpdated(){this.props.onSessionFilterUpdated&&this.props.onSessionFilterUpdated(this.state.filterTime)}},{key:"filterTime",value:function filterTime(f){var g=f.target.value;location.hash="",this.setState({filterTime:g},this.onSessionFilterUpdated)}},{key:"onRemoveStudent",value:function onRemoveStudent(f){this.props.onRemoveStudent&&this.props.onRemoveStudent(f)}},{key:"findSession",value:function findSession(f){location.hash="#"+f.target.value}},{key:"render",value:function render(){var j=this,f=this.props.data||[],g=this.state.codes||[],h=f.filter(function(k){return k.timeslot&&k.timeslot.code==j.state.filterTime}).sort(function(k,l){return k.name<l.name?-1:l.name<k.name?1:0});return React.createElement("div",null,React.createElement("div",{className:"form-group"},React.createElement("label",null,"Session Times"),React.createElement("select",{className:"form-control",onChange:this.filterTime,value:this.state.filterTime},g.sort(function(k,l){return k.startTime<l.startTime?0:(k.endTime<l.endTime?0:1);}).map(function(k){return React.createElement("option",{value:k.code},k.name)}))),React.createElement("div",null),React.createElement("div",{style:{maxHeight:"450px",overflowY:"scroll",paddingBottom:"200px"}},h.map(function(k){return React.createElement(Session,{data:k,students:j.props.students,onRemoveStudent:j.onRemoveStudent})})))}}]),d}(React.Component);Sessions.defaultProps={data:[]};var mountNode=document.getElementById("react-mount"),App=function(c){function d(f){_classCallCheck(this,d);var g=_possibleConstructorReturn(this,(d.__proto__||Object.getPrototypeOf(d)).call(this,f));return g.state={loading:!0,error:null,usedSession:!1},g.onRemoveStudent=g.onRemoveStudent.bind(g),g.onSessionFilterUpdated=g.onSessionFilterUpdated.bind(g),g}return _inherits(d,c),_createClass(d,[{key:"componentDidMount",value:function componentDidMount(){var g=this,f=function(){var h=reactDragula({accepts:function accepts(k,l){if("student-list"==l.id)return!1;var o=l.getAttribute("data-session-id"),p=g.state.blockedSessions.find(function(r){return r==o});if(p)return!1;var q=g.state.usedSession;return!q},copy:!0,invalid:function invalid(k){var m=k.getAttribute("data-student-id");if(!m)return!1;var n=g.state.students.find(function(p){return p.id==m}),o=g.state.sessions.filter(function(p){return p.timeslot.code==g.state.sessionFilter}).find(function(p){return n.sessionIds.find(function(q){return p.id==q})});return o},isContainer:function isContainer(k){return k.classList.contains("dragula-container")},revertOnSpill:!0});h.on("drag",function(k){var m=k.getAttribute("data-student-id"),n=g.state.students.find(function(p){return p.id==m}),o=n.sessionIds;g.setState({blockedSessions:o})}),h.on("dragend",function(){g.setState({blockedSessions:[]})}),h.on("drop",function(k,l){var o=k.getAttribute("data-student-id"),p=g.state.students,q=p.find(function(t){return t.id==o}),r=l.getAttribute("data-session-id");g.setState({thinking:!0},function(){return $.ajax("./api/sessions/"+r+"/students/"+o+"/add",{method:"POST"}).then(function(t){if(t.error)return void alert(t.message);var u=g.state.students,v=u.find(function(A){return A.id==o});v.sessionIds.push(r);var w=g.state.sessions,z=w.find(function(A){return A.id==r});z.studentCount++,g.setState({students:u,sessions:w})},function(t){alert(t.message)}).fail(function(t){alert(t.message)}).always(function(){g.setState({thinking:!1})})}),h.cancel(!0)})};$.ajax("./api/data").then(function(h){g.setState({students:h[0],sessions:h[1]},f)},function(h){g.setState({error:{source:"displaying session and student data",err:h}})}).fail(function(h){g.setState({error:{source:"downloading session and student data",err:h}})}).always(function(){g.setState({loading:!1})})}},{key:"onSessionFilterUpdated",value:function onSessionFilterUpdated(f){this.setState({sessionFilter:f})}},{key:"onRemoveStudent",value:function onRemoveStudent(f){var g=this.state.students,h=g.find(function(n){return n.id==f.studentId}),j=this.state.sessions,k=j.find(function(n){return n.id==f.sessionId});k.studentCount--;var l=h.sessionIds.filter(function(n){return n!=k.id}),m=h.sessionTimes.filter(function(n){return n!=k.timeslot.code});h.sessionIds=l,h.sessionTimes=m,this.setState({students:g,sessions:j})}},{key:"render",value:function render(){return React.createElement("div",{className:"container"},React.createElement("div",{className:"row"},this.state.error&&React.createElement("div",{className:"alert alert-danger"},"Error while ",this.state.error.source)),React.createElement("div",{className:"row"},React.createElement("div",{className:"col-xs-8"},React.createElement(Sessions,{data:this.state.sessions,students:this.state.students,loading:this.state.loading,onSessionFilterUpdated:this.onSessionFilterUpdated,onRemoveStudent:this.onRemoveStudent})),React.createElement("div",{className:"col-xs-4",style:{position:"fixed",right:"10px"}},React.createElement(Students,{data:this.state.students,sessions:this.state.sessions,loading:this.state.loading,sessionFilter:this.state.sessionFilter}))))}}]),d}(React.Component);ReactDOM.render(React.createElement(App,null),mountNode);
/*
class Student extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            expanded: false,
            sessionFilter: null
        }
        this.remove = this.remove.bind(this);
        this.onRemove = this.onRemove.bind(this);
        this.onExpand = this.onExpand.bind(this);
    }

    onRemove(e) {
        // TODO: Thinking callback
        if (this.props.onRemove)
            this.props.onRemove(e);
    }

    onExpand(e) {
        this.setState({
            expanded: !this.state.expanded
        });
    }

    remove() {
        if (this.state.thinking) return;
        var sessionId = this.props.sessionId;
        var studentId = this.props.data.id;
        var method = "POST";
        this.setState({thinking: true},
            () => $.ajax(`./api/sessions/${sessionId}/students/${studentId}/remove`, {method})
                .then(res => {
                    this.onRemove({sessionId, studentId});
                }, err => {
                    alert("error");
                })
                .fail(err => {
                    alert("error");
                })
                .always(() => {
                    this.setState({
                        thinking: false
                    });
                })
        );
    }

    render() {
        var data = this.props.data;
        var isFiltered = this.props.activeSessions.find(x=>data.sessionIds.find(y=>y==x.id));
        if (!this.props.removable && isFiltered) return null;
        var button = this.props.removable ? (
            <button className="btn btn-default btn-xs pull-right" style={{border: 'none', marginRight: '-5px'}}
                    onClick={this.remove}>
                <i className="glyphicon glyphicon-remove" style={{fontSize: '9px', paddingRight: '1px'}}></i>
            </button>
        ) : (
            null
        );
        var handle = this.props.removable ? (
            null
        ) : isFiltered ? (
            null
        ) : (
            <i className="glyphicon glyphicon-th" style={{marginRight: '7px'}}></i>
        );

        return (
            <li className="list-group-item" data-student-id={data.id}>
                { handle }
                { data.name }
                { button }
            </li>

        )
    }
}
Student.defaultProps = {
    activeSessions: []
};
class Students extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hideFullStudents: false,
            sessionFilter: null
        }
    }

    render() {
        var sessionFilter = this.props.sessionFilter;
        var activeSessions = this.props.sessions.filter(x=>x.timeslot.code == sessionFilter);

        return (
            <div class="row" style={{overflowY: 'scroll', height: '500px', paddingBottom: '250px'}}>
                <ul className="list-group dragula-container student-list">
                    {
                        this.props.data
                            .sort((a, b) => a.name < b.name ? -1 : b.name < a.name ? 1 : 0)
                            .sort((a, b) => a.sessionTimes.find(x=>x==sessionFilter) ? 1 : -1)
                            .map((x, i) => <Student data={x} activeSessions={activeSessions} sessionFilter={sessionFilter}/>)
                    }
                </ul>
            </div>
        );
    }
}
Students.defaultProps = {
    data: [],
    sessions: []

}
class Session extends React.Component {
    constructor(props) {
        super(props);
        this.onRemoveStudent = this.onRemoveStudent.bind(this);
    }

    onRemoveStudent(e) {
        if (this.props.onRemoveStudent)
            this.props.onRemoveStudent(e);
    }

    render() {
        var data = this.props.data;
        var myStudents = this.props.students.filter(x=>x.sessionIds.find(y=>y == data.id));
        var capPercent = (data.studentCount / data.capacity * 100);
        var caplabel = capPercent > 0.2 ? (data.studentCount + '/' + data.capacity) : null;
        return (
            <li className="list-group-item dragula-container" data-session-id={data.id} style={{margin: '10px 0'}}>
                <a name={data.id} style={{position:'absolute', top: '-10px'}}></a>
                { data.name }
                {
                    myStudents.map(x=><Student data={x} removable={true} sessionId={data.id}
                                               onRemove={this.onRemoveStudent}/>)
                }
                <div className="progress" style={{marginTop: '15px', marginBottom: '0px'}}>
                    <div className="progress-bar"
                         style={{width: capPercent + '%'}}>
                        {caplabel}
                    </div>
                </div>
            </li>
        )
    }
}
Session.defaultProps = {
    students: []
}
class Sessions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: null
        };
        this.filterTime = this.filterTime.bind(this);
        this.onRemoveStudent = this.onRemoveStudent.bind(this);
        this.onSessionFilterUpdated = this.onSessionFilterUpdated.bind(this);
    }

    componentWillReceiveProps(props) {
        if (this.state.codes) return;
        if (!props.data || !props.data.length) return;
        var codes = [];
        props.data
            .map(x=>x.timeslot)
            .filter(x=>x)
            .forEach(x=> {
                if (!codes.find(y=>y.code == x.code))
                    codes.push(x);
            });
        codes = codes.sort((a, b)=>a.code < b.code ? -1 : b.code < a.code ? 1 : 0);
        var filterTime = null;
        if (codes.length) filterTime = codes[0].code;
        this.setState({codes, filterTime}, this.onSessionFilterUpdated);
    }

    onSessionFilterUpdated() {
        if (this.props.onSessionFilterUpdated)
            this.props.onSessionFilterUpdated(this.state.filterTime);
    }

    filterTime(e) {
        var filterTime = e.target.value;
        location.hash = '';
        this.setState({filterTime}, this.onSessionFilterUpdated);
    }

    onRemoveStudent(e) {
        if (this.props.onRemoveStudent)
            this.props.onRemoveStudent(e);
    }

    findSession(e) {
        location.hash = "#" + e.target.value;
    }

    render() {
        var data = this.props.data || [];
        var codes = this.state.codes || [];
        var sessions = data.filter(x=>x.timeslot && x.timeslot.code == this.state.filterTime)
            .sort((a, b)=>a.name < b.name ? -1 : b.name < a.name ? 1 : 0);
        return (
            <div>
                <div className="form-group">
                    <label>Session Times</label>
                    <select className="form-control" onChange={this.filterTime} value={this.state.filterTime}>
                        {
                            codes.map(x=><option value={x.code}>{x.name}</option>)
                        }
                    </select>
                </div>
                <div>
                </div>
                <div className="row" style={{maxHeight: '450px', overflowY: 'scroll', paddingBottom: '200px'}}>
                    {
                        sessions.map(x => <Session data={x} students={this.props.students}
                                               onRemoveStudent={this.onRemoveStudent}/>)
                    }
                </div>
            </div>
        )
    }
}
Sessions.defaultProps = {
    data: []
};

var mountNode = document.getElementById("react-mount");

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            error: null,
            usedSession: false
        };
        this.onRemoveStudent = this.onRemoveStudent.bind(this);
        this.onSessionFilterUpdated = this.onSessionFilterUpdated.bind(this);
    }

    componentDidMount() {

        var loadDragula = () => {
            let options = {
                accepts: (el, target, source, sibling) => {
                    if (target.id == 'student-list') return false;
                    var sessionId = target.getAttribute('data-session-id');
                    var isBlocked = this.state.blockedSessions.find(x=>x == sessionId);
                    if (isBlocked) return false;
                    var usedSession = this.state.usedSession;
                    if (usedSession) return false;
                    return true;
                },
                copy: true,
                invalid: (el, handle) => {
                    var studentId = el.getAttribute('data-student-id');
                    if (!studentId) return false;
                    var student = this.state.students.find(x=>x.id == studentId);
                    var allocated = this.state.sessions
                        .filter(x=>x.timeslot.code == this.state.sessionFilter)
                        .find(x=>student.sessionIds.find(y=>x.id == y));
                    return allocated;
                },
                isContainer: (el) => {
                    return el.classList.contains('dragula-container');
                },
                revertOnSpill: true
            };
            var drake = reactDragula(options);
            drake.on('drag', (el, source) => {
                var studentId = el.getAttribute('data-student-id');
                var student = this.state.students.find(x=>x.id == studentId);
                var sessionIds = student.sessionIds;
                this.setState({
                    blockedSessions: sessionIds
                });
            });
            drake.on('dragend', (el, container, source) => {
                this.setState({
                    blockedSessions: []
                });
            });
            drake.on('drop', (el, target, source, sibling) => {
                var studentId = el.getAttribute('data-student-id');
                var students = this.state.students;
                var student = students.find(x=>x.id == studentId);
                var sessionId = target.getAttribute('data-session-id');
                var method = 'POST';
                this.setState({
                        thinking: true
                    },
                    () => $.ajax(`./api/sessions/${sessionId}/students/${studentId}/add`, {method})
                        .then(res => {
                            if (res.error) {
                                alert(res.message);
                                return;
                            }
                            var students = this.state.students;
                            var student = students.find(x=>x.id == studentId);
                            student.sessionIds.push(sessionId);
                            var sessions = this.state.sessions;
                            var session = sessions.find(x=>x.id == sessionId);
                            session.studentCount++;
                            this.setState({
                                students,
                                sessions
                            });
                        }, err => {
                            alert(err.message);
                        })
                        .fail(err => {
                            alert(err.message);
                        })
                        .always(() => {
                            this.setState({
                                thinking: false
                            })
                        })
                );

                drake.cancel(true);
            });

        };

        $.ajax("./api/data")
            .then(data => {
                this.setState({
                    students: data[0],
                    sessions: data[1]
                }, loadDragula);
            }, err => {
                this.setState({
                    error: {
                        source: 'displaying session and student data',
                        err: err
                    }
                });
            })
            .fail(err=> {
                this.setState({
                    error: {
                        source: 'downloading session and student data',
                        err: err
                    }
                });
            })
            .always(()=> {
                this.setState({loading: false});
            });

    }

    onSessionFilterUpdated(value) {
        this.setState({
            sessionFilter: value
        });
    }

    onRemoveStudent(e) {
        var students = this.state.students;
        var student = students.find(x=>x.id == e.studentId);
        var sessions = this.state.sessions;
        var session = sessions.find(x=>x.id == e.sessionId);
        session.studentCount--;
        var sessionIds = student.sessionIds.filter(x=>x != session.id);
        var sessionTimes = student.sessionTimes.filter(x=>x != session.timeslot.code);
        student.sessionIds = sessionIds;
        student.sessionTimes = sessionTimes;
        this.setState({
            students,
            sessions
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    {
                        this.state.error && (
                            <div className="alert alert-danger">
                                Error while { this.state.error.source }
                            </div>
                        )
                    }
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <Sessions data={this.state.sessions} students={this.state.students} loading={this.state.loading}
                                  onSessionFilterUpdated={this.onSessionFilterUpdated}
                                  onRemoveStudent={this.onRemoveStudent}/>
                    </div>
                    <div className="col-xs-4" style={{position: 'fixed', right: '10px'}}>
                        <Students data={this.state.students} sessions={this.state.sessions} loading={this.state.loading} sessionFilter={this.state.sessionFilter} />
                    </div>
                </div>
            </div>
        );
    }

}

ReactDOM.render(<App />, mountNode);
*/
