package filters;

import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.Map;
import javax.inject.Inject;
import akka.stream.Materializer;
import play.Logger;
import play.mvc.*;
import play.routing.Router.Tags;

public class RoutedLoggingFilter extends Filter {

    @Inject
    public RoutedLoggingFilter(Materializer mat) {
        super(mat);
    }

    @Override
    public CompletionStage<Result> apply(
            Function<Http.RequestHeader, CompletionStage<Result>> nextFilter,
            Http.RequestHeader requestHeader) {
        long startTime = System.currentTimeMillis();
        return nextFilter.apply(requestHeader).thenApply(result -> {
            Map<String, String> tags = requestHeader.tags();
            String actionMethod = requestHeader.uri();
            long endTime = System.currentTimeMillis();
            long requestTime = endTime - startTime;

            Logger.trace("{} took {}ms and returned {}",
                    actionMethod, requestTime, result.status());

	    return result;

        });
    }
}
