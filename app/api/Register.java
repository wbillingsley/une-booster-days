package api;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import controllers.Application;
import models.SessionTime;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.SchoolService;
import service.SessionService;
import service.StudentService;
import service.UserService;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Restrict(@Group(Application.USER_ROLE))
public class Register extends ApiController {

    @Inject
    public Register(StudentService studentService, SchoolService schoolService, SessionService sessionService, PlayAuthenticate auth) {
        super(studentService, schoolService, sessionService, auth);
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result add(Long id, Long studentId) {
        return manageSession(id, studentId, true);
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result data() {
        AuthUser authUser = auth.getUser(session());
        models.School school = schoolService.findByAuthUserIdentity(authUser);
        List<models.Student> students = school.students.stream()
                .filter(x->!x.isTeacher)
                .collect(Collectors.toList());
        List<models.Session> sessions = sessionService.getAll();
        // Cleanse student lists for current user
        sessions.stream().forEach(x->{
            x.students = x.students.stream()
                    .map(y->{
                        if (y.school.id != school.id) {
                            models.Student student = new models.Student();
                            student.id = y.id;
                            student.school = new models.School();
                            student.school.teacher = new User();
                            return student;
                        }
                        return y;
                    })
                    .collect(Collectors.toList());
        });
        return ok(Json.toJson(new Object[] { students, sessions }));
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result remove(Long id, Long studentId) {
        return manageSession(id, studentId, false);
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result capacity(Long id) {
        models.Session session = sessionService.findById(id);
        if (session == null)
            return error("Session not found");
        return success(session.students.size());
    }

    /* Private */

    private Result manageSession(Long id, Long studentId, Boolean add) {
        models.Session session = sessionService.findById(id);
        if (session == null)
            return error("Session not found");
        if (session.students.size() >= session.capacity && add)
            return error("Session is full");
        Date stopDate = null;
        try {
            stopDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm")).parse("2019-05-28 08:00");
            Date currDate = new Date();
            if(true || currDate.after(stopDate)){
                return error("It is after the cut-off for registrations");
            }
        } catch (ParseException e) {
            return error("It is after the cut-off for registrations");
        }
        AuthUser authUser = auth.getUser(session());
        models.Student student = studentService.findForAuthUserIdentity(studentId, authUser);
        if (student == null)
            return error("Student not found");
        Boolean inSession = session.students.contains(student);
        if (inSession && add) return error("Student already in session");
        if (!inSession && !add) return error("Student not found in session");
        if (add) {
            session.students.add(student);
            Logger.info("Added student {} to session {}", String.valueOf(studentId), String.valueOf(id));
        } else {
            student.sessions.remove(session);
            Logger.info("Removed student {} from session {}", String.valueOf(studentId), String.valueOf(id));
            student.save();
            if (session.students.contains(student))
            session.students.remove(student);
        }
        session.save();

        return success(session.students.size());
    }

    private Result success(int size) {
        ApiResponse<Integer> response = new ApiResponse<>();
        response.error = false;
        response.data = size;
        return ok(Json.toJson(response));
    }

    private Result error(String message) {
        Logger.error(message);
        ApiResponse<Integer> response = new ApiResponse<>();
        response.error = true;
        response.message = message;
        return ok(Json.toJson(response));
    }

}
