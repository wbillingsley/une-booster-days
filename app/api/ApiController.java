package api;

import com.feth.play.module.pa.PlayAuthenticate;
import play.mvc.Controller;
import service.SchoolService;
import service.SessionService;
import service.StudentService;

public class ApiController extends Controller {

    protected class ApiResponse<T> {
        public String message;
        public Boolean error;
        public T data;
    }

    protected final StudentService studentService;
    protected final SchoolService schoolService;
    protected final SessionService sessionService;
    protected final PlayAuthenticate auth;

    public ApiController(final StudentService studentService,
                    final SchoolService schoolService,
                    final SessionService sessionService, final PlayAuthenticate auth) {
        this.studentService = studentService;
        this.schoolService = schoolService;
        this.sessionService = sessionService;
        this.auth = auth;
    }

}
