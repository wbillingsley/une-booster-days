package api;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import controllers.Application;
import models.School;
import models.Session;
import models.Student;
import play.libs.Json;
import play.mvc.Result;
import service.SchoolService;
import service.SessionService;
import service.StudentService;

import javax.inject.Inject;
import java.util.List;

@Restrict(@Group(Application.ADMIN_ROLE))
public class Report extends ApiController {

    @Inject
    public Report(
            StudentService studentService,
            SchoolService schoolService,
            SessionService sessionService,
            PlayAuthenticate auth) {
        super(studentService, schoolService, sessionService, auth);
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result data() {

        List<School> schools = schoolService.getAll();
        List<Session> sessions = sessionService.getAll();
        List<models.Student> students = studentService.getAll();
        return ok(Json.toJson(new Object[] { schools, sessions, students }));

    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result schools() {
        //List<School> schools = schoolService.getWithActiveTeacher();
        //return ok(Json.toJson(schools));
        return ok();
    }

    @Restrict(@Group(Application.ADMIN_ROLE))
    public Result students() {
        //List<Student> students = studentService.getAllWithActiveTeacher();
        //return ok(Json.toJson(students));
        return ok();
    }

}
